#! /usr/bin/python3 
import os 
import re 
import numpy as np 

#### Do Pairwise Global Alignment
#### Seq in fasta format
#### Given BLOSUM62 matrix, Gap as 10 penalty

def GlobalAlignment(Seq1_fasta, Seq2_fasta):
    ## Remove header , newline 
    Seq1_slices = re.split("\n", Seq1_fasta)
    Seq2_slices = re.split("\n", Seq2_fasta)

    Seq1_string = ''
    Seq2_string = ''
    for each_part in Seq1_slices:
        if each_part.startswith(">") == 0:
            Seq1_string += each_part
    for each_part in Seq2_slices:
        if each_part.startswith(">") == 0:
            Seq2_string += each_part
    ## Given BLOSUM62 value
    ## https://www.ncbi.nlm.nih.gov/Class/FieldGuide/BLOSUM62.txt
    ## 23 by 23
    Matrix_Blosum62 =\
     np.matrix(' 4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0 -4;\
                -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1 -4 ;\
                -2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1 -4 ;\
                -2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1 -4 ;\
                 0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2 -4 ;\
                -1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1 -4 ;\
                -1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 ;\
                 0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1 -4 ;\
                -2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1 -4 ;\
                -1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1 -4 ;\
                -1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1 -4 ;\
                -1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1 -4 ;\
                -1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1 -4 ;\
                -2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1 -4 ;\
                -1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2 -4 ;\
                 1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0 -4 ;\
                 0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0 -4 ;\
                -3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2 -4 ;\
                -2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1 -4 ;\
                 0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1 -4 ;\
                -2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1 -4 ;\
                -1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 ;\
                 0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1 -4 ;\
                -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  1 ')
    ## Gap score 
    Gap = -10

    ## dictionary A.A. => Index in matrix 
    dic_aa_index = {'A': 0, 'R': 1, 'N': 2, 'D': 3, 'C': 4,
                    'Q': 5, 'E': 6, 'G': 7, 'H': 8, 'I': 9,
                    'L':10, 'K':11, 'M':12, 'F':13, 'P':14,
                    'S':15, 'T':16, 'W':17, 'Y':18, 'V':19,
                    'B':20, 'Z':21, 'X':22, '*':23 }

    ## Convert to upper size 
    Seq1_string = Seq1_string.upper()
    Seq2_string = Seq2_string.upper()

    ## length of sequence 
    m = len(Seq1_string)
    n = len(Seq2_string)

    ## calculate score 
    score_matrix = np.empty((m+1,n+1))

    for i in range(0, m+1,1):
        for j in range(0, n+1,1):
            if i == 0:
                score_matrix[i][j] = Gap * j
            if j == 0:
                score_matrix[i][j] = Gap * i
            if i != 0:
                if j != 0:
                    p = Matrix_Blosum62[dic_aa_index[Seq1_string[i-1]], dic_aa_index[Seq2_string[j-1]]]

                    score_matrix[i][j] = max( score_matrix[i-1][j] + Gap ,
                                          score_matrix[i-1][j-1] + p,
                                          score_matrix[i][j-1] + Gap )

    list_value = []
    if m > n:
        for i in range(0, m+1, 1):
            list_value.append(score_matrix[i][n])
    elif m < n:
        for i in range(0, n+1, 1):
            list_value.append(score_matrix[m][i])
    else:
        for i in range(0, n+1, 1):
            list_value.append(score_matrix[m][i])
        for j in range(0, m+1, 1):
            list_value.append(score_matrix[j][n])
    
    max_score = max(list_value)

    return max_score

def main():
    max_score = GlobalAlignment(Seq1_fasta = ">Seq1\nAAAA",
                    Seq2_fasta = ">Seq2\nAAAH")
    print(max_score)
    
    return 0

if __name__ == '__main__':
    main()