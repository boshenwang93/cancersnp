#! /usr/bin/python3

import re 
import os 

def ChangeHeaderName(fasta_file, new_name):
    f = open(fasta_file, 'r')
    for line in f:
        line = line.strip()
        if line.startswith(">"):
            new_header = ">" + new_name
            print(new_header)
        else:
            print(line)
    f.close()
    return 0 


def main(fasta_directory):

    for root, subdirs, files in os.walk(fasta_directory):
        for single_file in files:
            if single_file.endswith("fasta"):
                fasta_name = re.split("\.", single_file)[0]
                ChangeHeaderName(fasta_file = root+single_file,
                                 new_name = fasta_name)
    return 0 

if __name__ == "__main__":
    main(fasta_directory='/home/bwang/project/cancersnp/data/pph/fasta/var/')