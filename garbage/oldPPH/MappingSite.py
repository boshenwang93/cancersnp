#! /usr/bin/python3
import re
import os 


def ResidueMapping(site_info_file, mapping_folder):
    
    f = open(site_info_file)
    for line in f:
        line = line.strip()
        if re.search(',', line):
            list_slices = re.split(',', line)
            uniprot = list_slices[0].strip().upper()
            uniprot_seqnum = list_slices[1].strip().upper()
            uniprot_res = list_slices[2].strip().upper()

            pdb_w_chain = list_slices[3].strip().upper()
            mapping_file_path = mapping_folder + uniprot + '_' +\
                                pdb_w_chain + ',' + list_slices[4] +','+\
                                list_slices[5] + '.mapping'
            
            f_mapping = open(mapping_file_path, 'r')
            for each_line in f_mapping:
                up_seqnum = re.split(',', each_line)[-2]
                up_res = re.split(',', each_line)[-3]
                
                pdb_seqnum = re.split(',', each_line)[-1]

                if up_seqnum == uniprot_seqnum:
                    if up_res == uniprot_res:
                        line = line + pdb_seqnum
                        print(line)
            f_mapping.close()
    f.close()


    return 0 

ResidueMapping(site_info_file = '/home/bwang/project/CancerSNP/data/pph/DivPDB95.sites',
      mapping_folder = '/home/bwang/project/CancerSNP/main/calculation/alignment/mapping/')