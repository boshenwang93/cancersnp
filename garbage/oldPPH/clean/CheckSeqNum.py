#! /usr/bin/python3
import re 
import os

## obtain uniprot list, dictionary (uniprot => mutation patter, uniprot => mut sites)
def RetrieveProteinInfo(Polyphen_Input_Sum_file_path):
    ## initialize the list for unqiue uniprot 
    list_unique_uniprot = [] 

    ## read the Polyphen Input Summary File 
    f = open(Polyphen_Input_Sum_file_path, 'r')
    for line in f:
        slices = re.split("\t", line)
        uniprot_id = slices[0].strip().upper()

        if uniprot_id not in list_unique_uniprot:
            list_unique_uniprot.append(uniprot_id)

    f.close()
    
    ## initialize the dictionary for Uniprot => list of mutation
    ## sample dic['Q9Y462'] = ['622, E, D', '288, A, H']
    dic_uniprot_mutationlist = {}
    ## sample dic['Q9Y462'] = ['622, E', '288, A']
    dic_uniprot_site = {}

    ## iterate each protein 
    for each_uniprot in list_unique_uniprot:
        ## initialize the mutation list/ site list for current protein 
        mutation_list = []
        site_list = []

        f = open(Polyphen_Input_Sum_file_path, 'r')
        for line in f:
            slices = re.split("\t", line)
            uniprot_id = slices[0].strip().upper()
            seq_num = slices[1].strip()
            wild_type = slices[2].strip().upper()
            mutated_type = slices[3].strip().upper()

            mutation_entry = seq_num + ',' + wild_type + ',' + mutated_type
            site_entry = seq_num + ',' + wild_type

            if uniprot_id == each_uniprot:
                if mutation_entry not in mutation_list:
                    mutation_list.append(mutation_entry)

                if site_entry not in site_list:
                    site_list.append(site_entry)
        f.close()

        dic_uniprot_mutationlist[each_uniprot] = mutation_list
        dic_uniprot_site[each_uniprot] = site_list
   
    ## check the statistic for dataset 
    # count_uniprot = len(list_unique_uniprot)
    # count_mutations = 0
    # for key, value in dic_uniprot_mutationlist.items():
    #     count_mutations += len(value)
    # count_sites = 0
    # for key, value in dic_uniprot_site.items():
    #     count_sites += len(value)
    # print(count_uniprot, count_mutations, count_sites)

    ## return the value needed
    return list_unique_uniprot, dic_uniprot_mutationlist, dic_uniprot_site


def CheckSeqNum(uniprot_id, fasta_folder, dictionary_uniprot_site):
    uniprot_id = uniprot_id.upper()    
    
    ## Read fasta file and obtain all a.a. into a list 
    list_all_residues = [] 
    
    seq_string = ''
    file_path = fasta_folder + uniprot_id + '.fasta'
    f = open(file_path, 'r')
    for line in f:
        if line.startswith('>'):
            continue
        else:
            line = line.strip("\n")
            seq_string += line
    f.close()

    length_seq = len(seq_string)
    for i in range(0, length_seq, 1):
        seqnum = i + 1 
        new_entry = str(seqnum) + ',' + seq_string[i]
        list_all_residues.append(new_entry)

    ## Obtain the list for sites from PPH2
    list_mutated_sites = dictionary_uniprot_site[uniprot_id]
    
    for each_mutated_site in list_mutated_sites:
        if each_mutated_site not in list_all_residues:
            print(uniprot_id, each_mutated_site)

    return 0 

def main():

    ## Assign the pph summary file path 
    pph_file_directory = '/home/bwang/project/cancersnp/data/pph/raw/'
    div_pos_file = pph_file_directory + 'humdiv-2011_12.deleterious.pph.input'
    div_neg_file = pph_file_directory + 'humdiv-2011_12.neutral.pph.input'
    var_pos_file = pph_file_directory + 'humvar-2011_12.deleterious.pph.input'
    var_neg_file = pph_file_directory + 'humvar-2011_12.neutral.pph.input'
    
    ## obtain the uniprot list, mutation patter dictionary, sites dictionary 
    div_pos_list_uniprot, div_pos_dictionary_uniprot_mutation, div_pos_dictionary_uniprot_site =\
              RetrieveProteinInfo(Polyphen_Input_Sum_file_path = div_pos_file)
    div_neg_list_uniprot, div_neg_dictionary_uniprot_mutation, div_neg_dictionary_uniprot_site =\
              RetrieveProteinInfo(Polyphen_Input_Sum_file_path = div_neg_file)

    var_pos_list_uniprot, var_pos_dictionary_uniprot_mutation, var_pos_dictionary_uniprot_site =\
              RetrieveProteinInfo(Polyphen_Input_Sum_file_path = var_pos_file)
    var_neg_list_uniprot, var_neg_dictionary_uniprot_mutation, var_neg_dictionary_uniprot_site =\
              RetrieveProteinInfo(Polyphen_Input_Sum_file_path = var_neg_file)

    ## Assign fasta file path 
    div_fasta_dir = '/home/bwang/project/cancersnp/data/pph/fasta/div/'
    var_fasta_dir = '/home/bwang/project/cancersnp/data/pph/fasta/var/'
    
    ## Need to change the var/div & neg/pos
    for each_uniprot in var_pos_list_uniprot:
        CheckSeqNum(uniprot_id = each_uniprot, 
                    fasta_folder = var_fasta_dir,
                    dictionary_uniprot_site = var_pos_dictionary_uniprot_site)
    
    return 0

if __name__ == "__main__":
    main()
