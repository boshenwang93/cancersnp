#! /usr/bin/python3
import re 
import os

#### obtain the uniprot ID 
def RetrieveProteinInfo(Polyphen_Input_Sum_file_path):
    ## initialize the list for unqiue uniprot 
    list_unique_uniprot = [] 

    ## read the Polyphen Input Summary File 
    f = open(Polyphen_Input_Sum_file_path, 'r')
    for line in f:
        slices = re.split("\t", line)
        uniprot_id = slices[0].strip().upper()

        if uniprot_id not in list_unique_uniprot:
            if uniprot_id != '':
                list_unique_uniprot.append(uniprot_id)
    f.close()

    ## return the value needed
    return list_unique_uniprot

#### download the UniProt sequence file
def DownloadUniprotFasta(Uniprot_id, local_folder):
    Uniprot_id = Uniprot_id.upper()
    ## the web address for the Fasta & Canonical
    address_fasta_canonical = 'http://www.uniprot.org/uniprot/' +\
                               Uniprot_id + '.fasta'
    
    ## command for wget 
    cmd_wget = 'wget -P ' + local_folder + ' ' + address_fasta_canonical
    
    ## extute the cmd 
    os.system(cmd_wget)

    return 0

## MAIN Function 
def main():

    ## Assign the pph summary file path 
    pph_file_directory = '/home/bwang/project/cancersnp/data/pph/raw/'
    div_pos_file = pph_file_directory + 'humdiv-2011_12.deleterious.pph.input'
    div_neg_file = pph_file_directory + 'humdiv-2011_12.neutral.pph.input'
    var_pos_file = pph_file_directory + 'humvar-2011_12.deleterious.pph.input'
    var_neg_file = pph_file_directory + 'humvar-2011_12.neutral.pph.input'
    
    ## get the list of uniprot id 
    div_pos_list_uniprot =\
                  RetrieveProteinInfo(Polyphen_Input_Sum_file_path = div_pos_file)
    div_neg_list_uniprot =\
                  RetrieveProteinInfo(Polyphen_Input_Sum_file_path = div_neg_file)

    var_pos_list_uniprot =\
                  RetrieveProteinInfo(Polyphen_Input_Sum_file_path = var_pos_file)
    var_neg_list_uniprot =\
                  RetrieveProteinInfo(Polyphen_Input_Sum_file_path = var_neg_file)

    ## directory for uniprot fasta file 
    div_fasta_dir = '/home/bwang/project/cancersnp/data/pph/fasta/div/'
    var_fasta_dir = '/home/bwang/project/cancersnp/data/pph/fasta/var/'

    ## merge overlap uniprot between (pos, neg)
    list_div_uniprot = []
    for single_uniprot in div_pos_list_uniprot:
        if single_uniprot not in list_div_uniprot:
            list_div_uniprot.append(single_uniprot)
    for single_uniprot in div_neg_list_uniprot:
        if single_uniprot not in list_div_uniprot:
            list_div_uniprot.append(single_uniprot)

    list_var_uniprot = []
    for single_uniprot in var_pos_list_uniprot:
        if single_uniprot not in list_var_uniprot:
            list_var_uniprot.append(single_uniprot)
    for single_uniprot in var_neg_list_uniprot:
        if single_uniprot not in list_var_uniprot:
            list_var_uniprot.append(single_uniprot)

    ## downloading 
    for element in list_div_uniprot:
        DownloadUniprotFasta(Uniprot_id = element,
                             local_folder = div_fasta_dir)
    for element in list_var_uniprot:
        DownloadUniprotFasta(Uniprot_id = element,
                             local_folder = var_fasta_dir)

    ## check missing uniprot fasta 
    # for each_uniprot in list_var_uniprot:
    #     fasta_file_path = var_fasta_dir + each_uniprot + '.fasta'
    #     if os.path.exists(fasta_file_path) == 0:
    #         print(each_uniprot, "is missing")
    
    return 0 

## invoke main 
if __name__ == "__main__":
    main()