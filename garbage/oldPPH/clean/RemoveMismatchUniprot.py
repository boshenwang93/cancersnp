#! /usr/bin/python3 
import re 

def RemoveMismatchUniprot(Mismatch_file, original_pph_summary_file, out_file):
    ## read the mismatch file 
    list_mismatch_uniprot = []

    f = open(Mismatch_file, 'r')
    for line in f:
        slices = re.split('\s', line)
        uniprot = slices[0]
        if uniprot not in list_mismatch_uniprot:
            list_mismatch_uniprot.append(uniprot)
    f.close()

    out = open(out_file, 'w')

    f_raw = open(original_pph_summary_file, 'r')
    for line in f_raw:
        slices = re.split("\t", line)
        uniprot_id = slices[0].strip().upper()
        if uniprot_id not in list_mismatch_uniprot:
            out.write(line)
    f_raw.close()

    out.close()

    return 0

def main():
    RemoveMismatchUniprot(Mismatch_file = '/home/bwang/project/cancersnp/data/pph/raw/mismatch/mismatch_div_pos', 
                         original_pph_summary_file = '/home/bwang/project/cancersnp/data/pph/raw/humdiv-2011_12.deleterious.pph.input',
                         out_file = '/home/bwang/project/cancersnp/data/pph/raw/RemoveMismatch/DivPos')
     
    return 0 

if __name__ == "__main__":
    main()