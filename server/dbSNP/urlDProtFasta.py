#! /usr/bin/python3 
import re 
import json 
import os 
import urllib.request
import urllib.parse

###########################################################
#### API Key from gmail.com NCBI account ##################
#### eab17ba676a6bf4312f10f4b07fb1f883e08 #################
###########################################################

###########################################################
#### Step 1. Parse Json to obtain list of Prot Acc ########
#### Step 2. Follow URL protocol to download fasta file ###
###########################################################


## initilize list for storing prot acc number 
list_prot_acc = []

## parse json file
test_json_file = ''

## E-utilies use URL protocol 
## example for download fasta 
## efetch.fcgi?db=<database>&id=<uid_list>
## &rettype=<retrieval_type>
## &retmode=<retrieval_mode>

Eutil_url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'
file_name = 'efetch.fcgi?db=protein&acc=NP_005206.2&rettype=fasta&retmode=text'
req = urllib.request.urlretrieve(Eutil_url, file_name)

