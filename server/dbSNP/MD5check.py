import os 

#############################
#### MD5 checking ###########
#############################
def checkMD5 (local_dbSNP_folder):

    ## iterate local dbSNP folder, compare reference md5 with calculated md5
    for root, dirs, files in os.walk(local_dbSNP_folder):
        os.chdir(root)
        ## use reference md5 for checking
        for single_file in files:
            if single_file.endswith('.md5'): 
                cmd = 'md5sum -c ' + single_file 
                os.system(cmd)
    ## terminate the function
    return 0 

## MD5 checking 
local_storing_folder = '/data/database/dbSNP/B150GRCh38p7/'
checkMD5(local_dbSNP_folder = local_storing_folder)