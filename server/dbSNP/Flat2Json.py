#! /usr/bin/python3 
import os
import re 
import json 
import ast 

#######################################
#### Step 2. Parse Flat file ##########
#### COnvert to JSON format ###########
#### filter raw flat info #############
#### read line by line to save RAM ####
#######################################

#######################################
#### Exsample for JSON ################
#######################################

def Convert_Flat_to_JSON(Flat_file_path, out_json_file):

#### throw flat file info to raw tmp.txt ##################
    ## start file handle for tmp file which storing raw info
    out = open('tmp.txt', 'w')

    ## start file handle for Reading FLAT file
    f = open(Flat_file_path, 'r')

    for single_line in f:
        ## the components for missense SNP out info
        ## rsSNP ID, Class (missense), chromosome(1-23, X,Y), chrosome location

        ## read the rs ID line
        if single_line.startswith('rs'):
            rs_ID = re.split("\|",single_line)[0].strip()
            dic_rsid = {
                'rs_id': rs_ID
            }
            content = "\n" + str(dic_rsid) + ','
            out.write(content)

        ## rs SNP location/position
        ## may contain several lines depending on mapping?
        if single_line.startswith("CTG"):
            rs_chr_info = re.split("\|", single_line)[2].strip()
            rs_chr = re.split("=", rs_chr_info)[1]

            rs_chr_pos_info = re.split("\|", single_line)[3].strip()
            rs_chr_pos = re.split("=", rs_chr_pos_info)[1]
            
            if rs_chr_pos != '?':
                dic_chr_info = {
                    'chr': rs_chr,
                    'chr_pos': rs_chr_pos  
                }
                content = str(dic_chr_info) +  ','
                out.write(content)

        ## read the LOC info
        ## Attention LOC may contain several lines, use "|" for separate
        if single_line.startswith("LOC"):
            funClass = re.split("\|", single_line)[3].strip()
            function_class = re.split("=", funClass)[1].strip()
            fxn_class = function_class.lower()

            if fxn_class == "missense":
                ## some has mRNA, but no protein 
                ## Ignore model sequence which starts with "XP" , "XM"
                ## Only allow known sequence  "NM" "NP"
                prot_acc = ''
                try:
                    prot_acc_info = re.split("\|", single_line)[9].strip().upper()
                    prot_acc_candidate = re.split("=", prot_acc_info)[1].upper()
                    if prot_acc_candidate.startswith('N'):
                        prot_acc += prot_acc_candidate
                    else:
                        prot_acc += 'NA'
                except:
                    prot_acc += 'NA'

                gene_symbol = ''
                alt_residue = ''
                AA_position = ''
                if prot_acc != 'NA':
                    gene_symbol += re.split("\|", single_line)[1].strip().upper()
                    alt_residue_info = re.split("\|", single_line)[6].strip() 
                    alt_residue += re.split("=", alt_residue_info)[1].upper()                    
                    AA_position_info = re.split("\|", single_line)[7].strip()
                    AA_position += re.split("=", AA_position_info)[1]

                    dic_prot_info = {
                        'gene_symbol': gene_symbol,
                        'prot_acc': prot_acc,
                        'alt_AA': alt_residue,
                        'AA_position': AA_position
                    }

                    content = str(dic_prot_info) + ','
                    out.write(content)
    ## close file handle
    f.close()
    out.close()

#### convert raw tmp.txt to JSON file ######################
    ## json_out as file handle 
    json_out = open(out_json_file, 'a')
    ## open file handle for reading raw tmp.txt
    raw_f = open('tmp.txt', 'r')
    for line in raw_f:
        if re.search('prot_acc', line):
            dict_entry = {}
            rs_id_info = ''
            list_chr_position_info = []
            list_mutAA_info = []

            list_components = re.split('},', line)
            for slice in list_components:
                slice = slice + '}'
                if re.search('rs_id', slice):
                    dict_rs_id = ast.literal_eval(slice)
                    rs_id_info = dict_rs_id['rs_id']

                elif re.search('chr_pos', slice):
                    slice = ast.literal_eval(slice)
                    list_chr_position_info.append(dict(slice))

                elif re.search('prot_acc', slice):
                    slice = ast.literal_eval(slice)
                    list_mutAA_info.append(dict(slice))
        
            dict_entry['rs_id'] = rs_id_info
            dict_entry['chr_info'] = list_chr_position_info
            dict_entry['mutAA_info'] = list_mutAA_info

            json.dump(dict(dict_entry), json_out)
            json_out.write(",\n")
    raw_f.close()
    json_out.close()

    ## delete the tmp.txt file
    os.system('rm tmp.txt')

    ## exit the function 
    return 0


def Control_Json_Generator(dbSNP_flat_folder_path, json_out_file_path):
    ## iterating the Flat folder
    for file in os.listdir(dbSNP_flat_folder_path):    
        if file.endswith('flat'):
            file_path = dbSNP_flat_folder_path + file
            Convert_Flat_to_JSON(Flat_file_path = file_path,
                                 out_json_file = json_out_file_path)
            continue
        else:
            continue

    return 0


dbSNP_directory = '/data/database/dbSNP/B150GRCh38p7/'
json_file = '/data/database/dbSNP/json/all_dbSNP.json'

#### Invoke function for generating json file 
# out = open(json_file, 'a')
# out.write("[\n")
# out.close()
# Control_Json_Generator(dbSNP_flat_folder_path = dbSNP_directory,
#                        json_out_file_path = json_file)

# out = open(json_file, 'a')
# out.write("]")
# out.close()

# out = open(json_file, 'r')
# string_content = out.read()
# string_content = re.sub(",\n]", "\n]", string_content)
# out.close()

# cmd_rm = 'rm ' + json_file
# os.system(cmd_rm)

# out = open(json_file, 'w')
# out.write(string_content)
# out.close()

#### validate json file format     
f = open(json_file, 'r')
f_content = f.read()
try:
    json.loads(f_content)
    print('Json format Pass')
except:
    print('Not Pass Json format validation')
f.close()

