
############################################################################
# You should CHANGE THE NEXT THREE LINES to suit your local setup
############################################################################
MIRRORDIR=/data/database/PDB/             # your top level rsync directory
LOGFILE=/data/database/PDBsynclogs               # file for storing logs
RSYNC=/usr/bin/rsync                            # location of local rsync

##########################################################################################
# set the PDB node and Port parameter 
##########################################################################################
SERVER=rsync.wwpdb.org::ftp                                   # RCSB PDB server name
PORT=33444                                                    # port RCSB PDB server is using

############################################################################
# Rsync only the PDB format coordinates  /pub/pdb/data/structures/divided/pdb (Aproximately 20 GB)
############################################################################
${RSYNC} -rlpt -v -z --delete --port=$PORT ${SERVER}/data/structures/divided/pdb/ $MIRRORDIR > $LOGFILE 2>/dev/null