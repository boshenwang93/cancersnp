#! /usr/bin/python3 

import re

def GetPair(DelEdgeFile):

    list_contact_pair = []

    f = open(DelEdgeFile, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            list_line_slices = re.split(' ', line)
            atom1 = list_line_slices[0]
            atom2 = list_line_slices[1]
            
            list_s_atom1 = re.split(':', atom1)
            atom1_pm = '///'  + list_s_atom1[0] + '/' +\
                       list_s_atom1[1] + '/' +\
                       list_s_atom1[4]
                       
            list_s_atom2 = re.split(':', atom2)
            atom2_pm = '///'  + list_s_atom2[0] + '/' +\
                       list_s_atom2[1] + '/' +\
                       list_s_atom2[4]
            
            entry = atom1_pm + ',' + atom2_pm
            list_contact_pair.append(entry)
    f.close()

    return list_contact_pair

def GetSurfacePair(SurfaceEdgeFile):
    list_surface_pair = []
    f = open(SurfaceEdgeFile, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            iterator = re.finditer(':', line)
            iterator = tuple(iterator)
            count = len(iterator)
            
            if count == 8:
                list_line_slices = re.split(' ', line)
                atom1 = list_line_slices[0]
                atom2 = list_line_slices[1]
            
                list_s_atom1 = re.split(':', atom1)
                atom1_pm = '///'  + list_s_atom1[0] + '/' +\
                       list_s_atom1[1] + '/' +\
                       list_s_atom1[4]
                       
                list_s_atom2 = re.split(':', atom2)
                atom2_pm = '///'  + list_s_atom2[0] + '/' +\
                       list_s_atom2[1] + '/' +\
                       list_s_atom2[4]
            
                entry = atom1_pm + ',' + atom2_pm
                list_surface_pair.append(entry)
    f.close()
    return list_surface_pair


def DrawEdgeLine(list_all_alpha_pair, list_surface_pair, assigned_residue_chain_seqnum):
    from pymol import cmd 

    for each_pair in list_edges_pair:
        a1_info = re.split(',', each_pair)[0]
        a2_info = re.split(',', each_pair)[1]
        

        if a1_info.startswith(assigned_residue_chain_seqnum):
            cmd.select('a1', a1_info)
            cmd.select('a2', a2_info)
            cmd.distance('d', 'a1', 'a2')
        elif a2_info.startswith(assigned_residue_chain_seqnum):
            cmd.select('a1', a1_info)
            cmd.select('a2', a2_info)
            cmd.distance('d', 'a1', 'a2')
    
    return 0 

DrawEdgeLine(list_edges_pair = GetSurfacePair('6C1D_C.alpha.edges'),
             assigned_residue_chain_seqnum = '///C/232/N')

