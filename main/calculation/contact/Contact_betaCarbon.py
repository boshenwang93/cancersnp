#! /usr/bin/python3

#####################################
#### Written by Boshen Wang #########
#### Work for Monomer ###############
#### Calculate C-Beta contact #######
#### C-beta <= 8A  ##################
#### GLY as exception as CA #########
#####################################

########## Citation #################
##### PMID: 23760879  ###############
##### PMID: 21928322  ###############
##### PMID: 24267585  ###############

import math
import re
import os
from multiprocessing import Process, Pool

####### the dictionary residue_type[key] ==> heavy atoms[value]
###### residue type as string, heavy atoms as tuples
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}

list_amino_acids = [
    "ALA",
    "ARG",
    "ASN",
    "ASP",
    "CYS",
    "GLN",
    "GLU",
    "GLY",
    "HIS",
    "ILE",
    "LEU",
    "LYS",
    "MET",
    "PHE",
    "PRO",
    "SER",
    "THR",
    "TRP",
    "TYR",
    "VAL"
]

def capture_c_beta(pdb_file_path):
    # list for C-beta atom entry
    list_cb_atom_entry = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        ## column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            if re.match("GLY", residue):  ## Ignore case
                if re.match("CA", atom):
                    temp_cb_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                    list_cb_atom_entry.append(temp_cb_atom_entry)
            else:
                if re.match("CB", atom):
                    temp_cb_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                    list_cb_atom_entry.append(temp_cb_atom_entry)

    f.close()
    return list_cb_atom_entry


### INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

def calculate_contact(list_cb_atom, layer_distance):
    # Initialize the Output list which contain the contact less than 15A
    list_contact = []

    for host_atom in list_cb_atom:
        host_residue_name = re.split(",", host_atom)[0]
        host_residue_seq_number = re.split(",", host_atom)[1]
        host_residue_chain = re.split(",", host_atom)[2]

        host_identifier = host_residue_name + "," + host_residue_seq_number + "," + host_residue_chain

        host_atom_name = re.split(",", host_atom)[3]
        host_x = re.split(",", host_atom)[4]
        host_y = re.split(",", host_atom)[5]
        host_z = re.split(",", host_atom)[6]

        host_x = float(host_x)
        host_y = float(host_y)
        host_z = float(host_z)
        
        ## output line 
        out_entry = host_identifier + ","
        for i in range(0,20):
            for layer in range(1,4):
                left_cutoff = layer_distance * (layer - 1)
                right_cutoff = layer_distance * layer

                reference_residue = list_amino_acids[i]
                count = 0
            
                for guest_atom in list_cb_atom:
                    guest_residue_name = re.split(",", guest_atom)[0]
                    guest_residue_seq_number = re.split(",", guest_atom)[1]
                    guest_residue_chain = re.split(",", guest_atom)[2]
   
                    guest_identifier = guest_residue_name + "," + guest_residue_seq_number + "," + guest_residue_chain

                    guest_atom_name = re.split(",", guest_atom)[3]
                    guest_x = re.split(",", guest_atom)[4]
                    guest_y = re.split(",", guest_atom)[5]
                    guest_z = re.split(",", guest_atom)[6]

                    guest_x = float(guest_x)
                    guest_y = float(guest_y)
                    guest_z = float(guest_z)

                    if guest_identifier != host_identifier:
                        dx = host_x - guest_x
                        dy = host_y - guest_y
                        dz = host_z - guest_z

                        distance_square = math.pow(dx, 2) + math.pow(dy, 2) + math.pow(dz, 2)
                        distance  = math.sqrt(distance_square)

                        if  left_cutoff < distance <= right_cutoff:
                            if guest_residue_name == reference_residue:
                                count += 1
                out_entry += str(count) + ','
        
        list_contact.append(out_entry)

    return list_contact


def Merge( pdb_file ):
    distance = 3

    list_cb_atom = capture_c_beta(pdb_file_path = pdb_file)
    list_contact = calculate_contact(list_cb_atom, distance)

    pdb_id = pdb_file[-10:-4]
    
    out_file = '/home/bwang//project/cancersnp/main/calculation/contact/layer3A/' + pdb_id + '.3layers'
    out = open(out_file, 'w')
    for e in list_contact:
        outline = pdb_id + ',' + e + "\n"
        out.write(outline)
    out.close()

    return 0 
#### list_contact_8A
# [0] => Host Residue Name
# [1] => Host Residue Seq Number
# [2] => Host Residue Chain
# [3] =>  Neighbor Contact Number
# [4] =>  Short-Range Contact Number
# [5] =>  Medium-Range Contact Number
# [6] => Long-Range Contact Number

#################################################################
######### Iterate The Whole PDB file folder #####################
#################################################################

def main (assigned_folder):


    list_all_pdb_file = []
    for root, subdir, files in os.walk(assigned_folder):
        for file in files:
            if file.endswith("pdb"):
                file_path = assigned_folder + file
                list_all_pdb_file.append(file_path)

    with Pool(processes= 6) as pool:
        print(pool.map(Merge, list_all_pdb_file))

    return 0 

if __name__ == "__main__":
    main(assigned_folder = '/home/bwang/project/cancersnp/data/pph/pdb/DivPdb80/')
