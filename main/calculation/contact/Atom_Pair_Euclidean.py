#! /usr/bin/python3

#####################################
#### Calculate Euclidean Distance ###
#### Atom Pair Contact Profile ######
#### Written by Boshen Wang #########
#### Work for Monomer ###############
#####################################

import math
import re
import os
from multiprocessing import Process, Pool

# the dictionary residue_type[key] ==> heavy atoms[value]
# Add C terminal (OXT)
# residue type as string, heavy atoms as tuples
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB", "OXT"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1", "OXT"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2", "OXT"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2", "OXT"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG", "OXT"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2", "OXT"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2", "OXT"),
    "GLY": ("N", "CA", "C", "O", "OXT"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2", "OXT"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1", "OXT"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "OXT"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ", "OXT"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE", "OXT"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ", "OXT"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD", "OXT"),
    "SER": ("N", "CA", "C", "O", "CB", "OG", "OXT"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2", "OXT"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2", "OXT"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH", "OXT"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "OXT"),
}

#### Capture the heavy atom from PDB file
def capture_heavy_atom(pdb_file_path):
    # list for all heavy atom entry
    list_heavy_atom_entry = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            try:
                if atom in dic_residue_heavyatom[residue]:
                    tmp_atom_entry = residue + "," + residue_seq_number + "," +\
                                     chain + "," + atom + "," + x + "," + y + "," + z
                    list_heavy_atom_entry.append(tmp_atom_entry)
            except:
                print(pdb_file_path, 'has unrecognized residue')
    f.close()
    pdb_ID = pdb_file_path[-10:-4]
    return list_heavy_atom_entry, pdb_ID


# INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

def calculate_Euclidean(list_atom, pdbID):
    # Initialize the Output list which contain the contact less than 15A
    list_ED_atom_pair = []

    for host_atom in list_atom:
        host_residue_name = re.split(",", host_atom)[0]
        host_residue_seq_number = re.split(",", host_atom)[1]
        host_residue_chain = re.split(",", host_atom)[2]

        host_identifier = host_residue_name + "," + \
            host_residue_seq_number + "," + host_residue_chain

        host_atom_name = re.split(",", host_atom)[3]
        host_x = re.split(",", host_atom)[4]
        host_y = re.split(",", host_atom)[5]
        host_z = re.split(",", host_atom)[6]

        host_x = float(host_x)
        host_y = float(host_y)
        host_z = float(host_z)

        # output line
        for guest_atom in list_atom:
            guest_residue_name = re.split(",", guest_atom)[0]
            guest_residue_seq_number = re.split(",", guest_atom)[1]
            guest_residue_chain = re.split(",", guest_atom)[2]

            guest_identifier = guest_residue_name + "," + \
                guest_residue_seq_number + "," + guest_residue_chain

            guest_atom_name = re.split(",", guest_atom)[3]
            guest_x = re.split(",", guest_atom)[4]
            guest_y = re.split(",", guest_atom)[5]
            guest_z = re.split(",", guest_atom)[6]

            guest_x = float(guest_x)
            guest_y = float(guest_y)
            guest_z = float(guest_z)

            if guest_identifier != host_identifier:
                dx = host_x - guest_x
                dy = host_y - guest_y
                dz = host_z - guest_z

                distance_square = math.pow(
                    dx, 2) + math.pow(dy, 2) + math.pow(dz, 2)
                distance = math.sqrt(distance_square)

                if distance <= 15:
                    out_line = str(distance) + ',' + \
                        host_atom + ',' + guest_atom
                    list_ED_atom_pair.append(out_line)

    ## write the each pair contact pair to the new file
    out_file_path = pdbID + '.EDcontact'
    out = open(out_file_path, 'w')
    for each_pair in list_ED_atom_pair:
        out_line = each_pair + "\n"
        out.write(out_line)
    out.close()

    return 0

def MergeTwoSteps(pdb_file):
    all_atom_list, pdb_id = capture_heavy_atom(pdb_file_path = pdb_file)
    calculate_Euclidean(list_atom = all_atom_list, pdbID = pdb_id)
    return 0 

#################################################################
######### Iterate The Whole PDB file folder #####################
#################################################################
def main(assigned_folder):

    list_pdb_file_path = []
    for root, subdir, files in os.walk(assigned_folder):
        for file in files:
            if file.endswith("pdb"):
                file_path = assigned_folder + file
                list_pdb_file_path.append(file_path)

    with Pool(processes= 2) as pool:
        print(pool.map(MergeTwoSteps, list_pdb_file_path))
    
    return 0

if __name__ == "__main__":
    main(assigned_folder='/home/bwang/project/cancersnp/data/pph/pdb/DivPdb80/')
