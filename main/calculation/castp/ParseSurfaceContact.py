#! /usr/bin/python3

import re
import os
from multiprocessing import Process, Pool


def Obtain_Surface_Contact_Pair(SurfaceContact_file_path):
    # initalize the list for all unique residues
    list_unique_residues = []
    # initalize list for the mutable pairs
    list_residue_pairs = []

    PDB_id = re.split("\/", AlphaEdgeFile)[-1]
    PDB_id = re.split("\.alpha", PDB_id)[0] 

    f = open(AlphaEdgeFile, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            list_line_slices = re.split(' ', line)
            atom1 = list_line_slices[0]
            atom2 = list_line_slices[1]

            list_s_atom1 = re.split(':', atom1)
            residue1 = list_s_atom1[0] + ':' +\
                list_s_atom1[1] + ':' +\
                list_s_atom1[2]

            list_s_atom2 = re.split(':', atom2)
            residue2 = list_s_atom2[0] + ':' +\
                list_s_atom2[1] + ':' +\
                list_s_atom2[2]
            if residue1 not in list_unique_residues:
                list_unique_residues.append(residue1)
            if residue2 not in list_unique_residues:
                list_unique_residues.append(residue2)

            if residue1 != residue2:
                atom1 = residue1 + ':' + list_s_atom1[4]
                atom2 = residue2 + ':' + list_s_atom2[4]
                pair = residue1 + ',' + residue2
                list_residue_pairs.append(pair)
    f.close()

    list_amino_acids = [
        "ALA",
        "ARG",
        "ASN",
        "ASP",
        "CYS",
        "GLN",
        "GLU",
        "GLY",
        "HIS",
        "ILE",
        "LEU",
        "LYS",
        "MET",
        "PHE",
        "PRO",
        "SER",
        "THR",
        "TRP",
        "TYR",
        "VAL"
    ]
     

    for each_residue in list_unique_residues:
        out_entry = PDB_id + ',' + each_residue

        for i in range(0, len(list_amino_acids)):
            every_aa = list_amino_acids[i]
            count = 0 

            for each_pair in list_residue_pairs:
                pair_res1 = re.split(',', each_pair)[0]
                pair_res2 = re.split(',', each_pair)[1]

                if pair_res1 == each_residue:
                    res2_triletter = re.split(':', pair_res2)[-1]
                    if res2_triletter == every_aa:
                        count += 1
                
                if pair_res2 == each_residue:
                    res1_triletter = re.split(':', pair_res1)[-1]
                    if res1_triletter == every_aa:
                        count += 1
    return 0