#! /usr/bin/python3 

import re
import os 
from multiprocessing import Process, Pool


## list for all amino acid 
list_amino_acids = [
    "ALA",
    "ARG",
    "ASN",
    "ASP",
    "CYS",
    "GLN",
    "GLU",
    "GLY",
    "HIS",
    "ILE",
    "LEU",
    "LYS",
    "MET",
    "PHE",
    "PRO",
    "SER",
    "THR",
    "TRP",
    "TYR",
    "VAL"
]


def Get_Edge_Pair(Alpha_Edge_File):
    ## Extract PDB 
    PDB_ID = Alpha_Edge_File[-18:-12]

    ## initialize vector 
    list_vector = [] 

    ## initialize edge pair for inter/intra residue
    list_interRes_edge_pair = []

    ## initialize the unique atom/residue
    list_unique_atom = []
    list_unique_residue = []

    f = open(Alpha_Edge_File, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            iterator = re.finditer(':', line)
            iterator = tuple(iterator)
            count = len(iterator)
            
            if count == 8:
                list_line_slices = re.split(' ', line)
                atom1 = list_line_slices[0]
                atom2 = list_line_slices[1]

                list_s_atom1 = re.split(':', atom1)
                res_1 = list_s_atom1[0] + ':' +\
                        list_s_atom1[1] + ':' +\
                        list_s_atom1[2]
                       
                list_s_atom2 = re.split(':', atom2)
                res_2 = list_s_atom2[0] + ':' +\
                        list_s_atom2[1] + ':' +\
                        list_s_atom2[2]
                
                if atom1 not in list_unique_atom:
                    list_unique_atom.append(atom1)
                if atom2 not in list_unique_atom:
                    list_unique_atom.append(atom2)
                if res_1 not in list_unique_residue:
                    list_unique_residue.append(res_1)
                if res_2 not in list_unique_residue:
                    list_unique_residue.append(res_2)

                edge = atom1 + ',' + atom2

                if res_1 != res_2:
                    list_interRes_edge_pair.append(edge)
    f.close()
    
    ## Dictionary key(residue)   => value (heavy atom by this residue)
    dic_residue_atom = {}
    for each_residue in list_unique_residue:
        tmp_list = []
        for each_atom in list_unique_atom:
            if each_atom.startswith(each_residue):
                tmp_list.append(each_atom)
        dic_residue_atom[each_residue] = tmp_list

    ## Use the adjacency list to represent atom => list of linked atoms
    dic_atom_linkedatom = {}
    for each_atom in list_unique_atom:
        tmp_list = []
        for each_edge in list_interRes_edge_pair:
            left_node = re.split(',', each_edge)[0]
            right_node = re.split(',', each_edge)[1]

            if left_node == each_atom:
                tmp_list.append(right_node)
            if right_node == each_atom:
                tmp_list.append(left_node)
        dic_atom_linkedatom[each_atom] = tmp_list
    
    ## Obtain the 3 layers residue composition by counting the residue
    for each_residue in list_unique_residue:
        list_atoms_current_residue = dic_residue_atom[each_residue]
        
        ## obtain the first layer connected 
        list_first_layer_atom = []
        list_first_layer_residue = [] 
        for each_atom in list_atoms_current_residue:
            for e in dic_atom_linkedatom[each_atom]:
                e_res_ID = re.split(':', e)[0] + ':' +\
                           re.split(':', e)[1] + ':' +\
                           re.split(':', e)[2]
                if e not in list_first_layer_atom:
                    list_first_layer_atom.append(e)
                if e_res_ID not in list_first_layer_residue:
                    list_first_layer_residue.append(e_res_ID)

        ## obtain the 2nd layer connect 
        list_second_layer_atom = []
        list_second_layer_residue = []
        for each_atom in list_first_layer_atom:
            for e in dic_atom_linkedatom[each_atom]:
                e_res_ID = re.split(':', e)[0] + ':' +\
                           re.split(':', e)[1] + ':' +\
                           re.split(':', e)[2]
                if e not in list_atoms_current_residue:
                    if e not in list_first_layer_atom:
                        if e not in list_second_layer_atom:
                            list_second_layer_atom.append(e)
                if e_res_ID != each_residue:
                    if e_res_ID not in list_first_layer_residue:
                        if e_res_ID not in list_second_layer_residue:
                            list_second_layer_residue.append(e_res_ID)

        ## obtain the 3rd layer 
        list_third_layer_atom = []
        list_third_layer_residue = []
        for each_atom in list_second_layer_atom:
            for e in dic_atom_linkedatom[each_atom]:
                e_res_ID = re.split(':', e)[0] + ':' +\
                           re.split(':', e)[1] + ':' +\
                           re.split(':', e)[2]
                if e not in list_atoms_current_residue:
                    if e not in list_first_layer_atom:
                        if e not in list_second_layer_atom:
                            if e not in list_third_layer_atom:
                                list_third_layer_atom.append(e)
                if e_res_ID != each_residue:
                    if e_res_ID not in list_first_layer_residue:
                        if e_res_ID not in list_second_layer_residue:
                            if e_res_ID not in list_third_layer_residue:
                                list_third_layer_residue.append(e_res_ID)
        ## for output
        out_entry = PDB_ID + ',' + each_residue + ','  
        for i in range(0,20):
            reference_residue = list_amino_acids[i]

            count_first = 0
            for e in list_first_layer_residue:
                e_res_type = re.split(':', e)[2]
                if e_res_type == reference_residue:
                    count_first += 1

            count_second = 0 
            for e in list_second_layer_residue:
                e_res_type = re.split(':', e)[2]
                if e_res_type == reference_residue:
                    count_second += 1 
            
            count_third = 0 
            for e in list_third_layer_residue:
                e_res_type = re.split(':', e)[2]
                if e_res_type == reference_residue:
                    count_third += 1 
            
            out_entry += str(count_first) + ',' +\
                         str(count_second) + ',' + str(count_third) + ','
        list_vector.append(out_entry)

    out_file_path = '/home/bwang/project/cancersnp/main/calculation/castp/layer/' + PDB_ID + '.3layerAE'
    out = open(out_file_path, 'w')
    for e in list_vector:
        outline = e + "\n" 
        out.write(outline)
    out.close()
    return 0

# list_vector_value = Get_Edge_Pair(Alpha_Edge_File = '1A5E_A.alpha.edges')

def main(assigned_folder):
    # iterating the pdb folder
    list_pdb_file = []

    pdb_directory = assigned_folder
    for root, dirs, files in os.walk(pdb_directory):
        for file in files:
            if file.endswith(".alpha.edges"):
                file_path = root + file
                list_pdb_file.append(file_path)
    return list_pdb_file

if __name__ == "__main__":
    list_pdb = main(assigned_folder = '/home/bwang/project/cancersnp/data/pph/pdb/DivPdb80/')
    
    with Pool(processes= 6) as pool:
        print(pool.map(Get_Edge_Pair, list_pdb))