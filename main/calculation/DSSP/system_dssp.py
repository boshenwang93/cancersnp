#! /usr/bin/python3
import re 
import os

#### iterating the pdb folder
pdb_directory = "/home/bwang/project/CancerSNP/data/pph/pdb/DivPdb95/"
dssp_directory = "/home/bwang/project/CancerSNP/main/calculation/DSSP/Div95/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        if file.endswith('poc.pdb') == 0:
            if file.endswith('pdb'):
                file_path = subdir + file
                pdbid = re.split('/', file)[-1]
                pdbid = re.split(".pdb", pdbid)[0]
          
                dssp_file_path = dssp_directory + pdbid + ".dssp"
        
                command_line = "mkdssp -i " + file_path +\
                               " -o " + dssp_file_path
                os.system(command_line)
