#! /usr/bin/python3
import re
import os 

#### dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V",
}


#### capture information from dssp output file
#### file name rule "PDB_ID.dssp"
#### secondary strcture, water contact number, etc

def capture_information_dssp(dssp_out_file):
    f = open(dssp_out_file, 'r')

    list_residue_ss_info = []

    for line in f:

        # delete the white space
        trim_line = line.strip()
        # capture the atom line
        if trim_line.endswith(".") == 0:
            if trim_line.startswith("#") == 0:

                seq_num = ''
                for i in range(5, 10, 1):
                    seq_num += line[i]
                seq_num = seq_num.strip()

                chain_id = line[11]

                residue_one = line[13]
                residue_one = residue_one.upper()

                residue_type = ''

                if residue_one == "b":
                    residue_type += "CYS"
                elif residue_one == "a":
                    residue_type += "CYS"
                else:
                    for k,v in dic_tri_single.items():
                        if v == residue_one:
                            residue_type += k
                        

                ss_type = line[16]
                # three types of helix
                if ss_type == "H":
                    ss_type = "Alpha_Helix"
                elif ss_type == "G":
                    ss_type = "3_10_Helix"
                elif ss_type == "I":
                    ss_type = "Pi_helix"
                # two types of beta sheet
                elif ss_type == "E":
                    ss_type = "Beta_sheet"
                elif ss_type == "B":
                    ss_type = "Beta_bridge"
                ## loop
                elif ss_type == "T":
                    ss_type = "HydrogenBondedTurn"
                # otherwise as loop
                else:
                    ss_type = "Loop"

                water_contact = ''
                for i in range(35, 38, 1):
                    water_contact += line[i]
                water_contact = water_contact.strip()

                phi = ''
                for i in range(104, 109, 1):
                    phi += line[i]
                phi = phi.strip()

                psi = ''
                for i in range(110, 115, 1):
                    psi += line[i]
                psi = psi.strip()

                bend_angle = ''
                for i in range(92, 97, 1):
                    bend_angle += line[i]
                bend_angle = bend_angle.strip()

                dihedral_angle = ''
                for i in range(98, 103, 1):
                    dihedral_angle += line[i]
                dihedral_angle = dihedral_angle.strip()

                # cosine of angle between C=O of residue I and C=O of residue I-1
                TCO = ''
                for i in range(85, 91, 1):
                    TCO += line[i]
                TCO = TCO.strip()

                # N-H donor & receptor
                d1 = ''
                for i in range(39, 50, 1):
                    d1 += line[i]
                d1 = d1.strip()
                d1_pos = re.split(",", d1)[0]
                d1_energy = re.split(",", d1)[1]

                r1 = ''
                for i in range(51, 62, 1):
                    r1 += line[i]
                r1 = r1.strip()
                r1_pos = re.split(",", r1)[0]
                r1_energy = re.split(",", r1)[1]

                d2 = ''
                for i in range(63, 74, 1):
                    d2 += line[i]
                d2 = d2.strip()
                d2_pos = re.split(",", d2)[0]
                d2_energy = re.split(",", d2)[1]

                r2 = ''
                for i in range(75, 85, 1):
                    r2 += line[i]
                r2 = r2.strip()
                r2_pos = re.split(",", r2)[0]
                r2_energy = re.split(",", r2)[1]

                current_residue_ss_info = residue_type + "," + chain_id + "," \
                    + seq_num + "," + ss_type + "," + water_contact \
                    + "," + phi + "," + psi + "," + bend_angle + "," + \
                    dihedral_angle + "," + TCO + "," + d1_pos + "," +\
                    d1_energy + "," + r1_pos + "," + r1_energy + "," +\
                    d2_pos + "," + d2_energy + "," + r2_pos + "," + r2_energy

                # print(current_residue_ss_info)
                list_residue_ss_info.append(current_residue_ss_info)

    f.close()

    return list_residue_ss_info

dssp_directory = "/home/bwang/project/CancerSNP/main/calculation/DSSP/Div80/"

for subdir, dirs, files in os.walk(dssp_directory):
    for file in files:
        dssp_path = subdir + file

        pdbid = ''
        for i in range(0, 6, 1):
            pdbid += file[i]
        
        list_dssp_info = capture_information_dssp(dssp_path)

        for element in list_dssp_info:
            out_line = pdbid + "," + element
            print(out_line)


# def output_loop_pdb(dssp_file, pdb_file):

#     list_loop_residue_identify = []

#     list_residue_ss_info = capture_information_dssp(dssp_file)

#     for each_residue in list_residue_ss_info:
#         residue_one_letter = re.split(",", each_residue)[0]
#         seq_number = re.split(",", each_residue)[2]
#         chain_id = re.split(",", each_residue)[1]
#         ss_type = re.split(",", each_residue)[3]

#         residue_identify = residue_one_letter + "," + seq_number + \
#             "," + chain_id
#         if ss_type == "Loop":
#             list_loop_residue_identify.append(residue_identify)
    
#     pdb_id = re.split("/",pdb_file)[-1]
#     pdb_id = re.split(".pdb",pdb_id)[0]
#     out_file_path = "/data/Dropbox/project/HD/feature/ss/loop_pdb/" + pdb_id + ".loop"

#     out  = open(out_file_path,"w")

#     f = open(pdb_file, "r")
#     for line in f:
#         line_record_identify = ""
#         # column 1-6 is the identfication for the line record
#         for i in range(0, 6, 1):
#             line_record_identify += line[i]
#         line_record_identify = line_record_identify.strip()

#         if line_record_identify == "ATOM":
#             # residue name
#             residue = ""
#             for i in range(17, 20, 1):
#                 residue += line[i]
#             residue = residue.strip()

#             # chain identifier
#             chain = line[21]

#             # residue sequence number
#             residue_seq_number = ""
#             for i in range(22, 26, 1):
#                 residue_seq_number += line[i]
#             residue_seq_number = residue_seq_number.strip()

#             atom_affiliated_residue_identify = residue + "," + residue_seq_number +\
#                                                 ","  + chain
            
#             if atom_affiliated_residue_identify in list_loop_residue_identify:
#                 out.write(line)
#     f.close()
#     out.close()


# pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
# dssp_directory = "/data/Dropbox/project/HD/feature/ss/dssp_out/"

# for subdir, dirs, files in os.walk(dssp_directory):
#     for file in files:
#         dssp_path = subdir + file

#         pdbid = ''
#         for i in range(0, 4, 1):
#             pdbid += file[i]
        
#         pdb_path = pdb_directory + pdbid + ".pdb"

#         output_loop_pdb(dssp_path,pdb_path)