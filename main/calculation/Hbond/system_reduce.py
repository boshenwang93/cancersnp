#! /usr/bin/python3

import os

# iterating the pdb folder
pdb_directory = "/home/bwang/project/cancersnp/data/pph/pdb/DivPdb95/"
H_add_directory = "/home/bwang/project/cancersnp/main/calculation/Hbond/Div95ReduceAddH/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:

        file_path = subdir + file
        H_add_file_path = H_add_directory + file
        
        if file.endswith('poc.pdb') == 0:
            if file.endswith('pdb'):
                command_line = "./reduce.3.23.130521.linuxi386 -FLIP " + file_path + ">" + H_add_file_path
                os.system(command_line)
