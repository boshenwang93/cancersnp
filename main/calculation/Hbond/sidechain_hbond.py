#! /usr/bin/python3

import re
import math
import os
import numpy

# the dictionary residue_type[key] ==> heavy atoms[value]
# residue type as string, heavy atoms as tuples

dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}

# the dictionary residue_type[key] ==> Hydrogen atoms[value]
# residue type as string, hydrogen atoms as tuples
dic_residue_sidechainhydrogen = {
    "ALA": ("HA", "1HB", "2HB", "3HB"),
    "ARG": ("HA", "1HB", "2HB", "1HG", "2HG", "1HD", "2HD", "HE", "1HH1", "2HH1", "1HH2", "2HH2"),
    "ASN": ("HA", "1HB", "2HB", "1HD2", "2HD2"),
    "ASP": ("HA", "1HB", "2HB"),
    "CYS": ("HA", "1HB", "2HB", "HG"),
    "GLN": ("HA", "1HB", "2HB", "1HG", "2HG", "1HE2", "2HE2"),
    "GLU": ("HA", "1HB", "2HB", "1HG", "2HG"),
    "GLY": ("1HA", "2HA"),
    "HIS": ("HA", "1HB", "2HB", "HD2", "HD2", "HE1"),
    "ILE": ("HA", "HB", "1HG1", "2HG1", "1HG2", "2HG2", "3HG2", "1HD1", "2HD1", "3HD1"),
    "LEU": ("HA", "1HB", "2HB", "HG", "1HD1", "2HD1", "3HD1", "1HD2", "2HD2", "3HD2"),
    "LYS": ("HA", "1HB", "2HB", "1HG", "2HG", "1HD", "2HD", "1HE", "2HE", "1HZ", "2HZ", "3HZ"),
    "MET": ("HA", "1HB", "2HB", "1HG", "2HG", "1HE", "2HE", "3HE"),
    "PHE": ("HA", "1HB", "2HB", "HD1", "HD2", "HE1", "HE2", "HZ"),
    "PRO": ("H2", 'H1', "HA", "1HB", "2HB", "1HG", "2HG", "1HD", "2HD"),
    "SER": ("HA", "1HB", "2HB", "HG"),
    "THR": ("HA", "HB", "HG1", "1HG2", "2HG2", "3HG2"),
    "TRP": ("HA", "1HB", "2HB", "HD1", "HE1", "HE3", "HZ2", "HZ3", "HH2"),
    "TYR": ("HA", "1HB", "2HB", "HD1", "HD2", "HE1", "HE2", "HH"),
    "VAL": ("HA", "HB", "1HG1", "2HG1", "3HG1", "1HG2", "2HG2", "3HG2"),
}


def capture_c_beta(pdb_file_path):
    # initilize amide list, hydrogen list, Oxygen list
    list_amide = []
    list_hydrogen = []
    list_oxygen = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:

        if line.startswith("ATOM"):
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # signle assitant identity charater
            sig_atom = line[76] + line[77]
            sig_atom = sig_atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            ## atom iden 
            atom_iden = line[77]

            temp_atom_entry = residue + "," + residue_seq_number + \
                "," + chain + "," + atom + "," + x + "," + y + "," + z

            if atom_iden == "N":
                list_amide.append(temp_atom_entry)
            elif sig_atom == "O":
                list_oxygen.append(temp_atom_entry)
            elif atom in dic_residue_sidechainhydrogen[residue]:
                list_hydrogen.append(temp_atom_entry)

    f.close()
    return list_amide, list_oxygen, list_hydrogen


# INDEX list_ATOM
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

# set the threshold for hydrogen bond as 3.5
cutoff_hbond = 3.5


def hydrogen_calculation(list_amide, list_oxygen, list_hydrogen):
    list_amide_hbond_info = []

    for N_atom in list_amide:
        N_residue = re.split(",", N_atom)[0] + "," +\
            re.split(",", N_atom)[1] + "," +\
            re.split(",", N_atom)[2]

        N_x = float(re.split(",", N_atom)[4])
        N_y = float(re.split(",", N_atom)[5])
        N_z = float(re.split(",", N_atom)[6])

        number_hbond = 0
        hbond_strength = 0

        for O_atom in list_oxygen:
            O_x = float(re.split(",", O_atom)[4])
            O_y = float(re.split(",", O_atom)[5])
            O_z = float(re.split(",", O_atom)[6])

            for H_atom in list_hydrogen:
                H_residue = re.split(",", H_atom)[0] + "," +\
                    re.split(",", H_atom)[1] + "," +\
                    re.split(",", H_atom)[2]

                H_x = float(re.split(",", H_atom)[4])
                H_y = float(re.split(",", H_atom)[5])
                H_z = float(re.split(",", H_atom)[6])

                dist_OH = math.sqrt(math.pow((O_x - H_x), 2) +
                                    math.pow((O_y - H_y), 2) +
                                    math.pow((O_z - H_z), 2))

                dist_NH = math.sqrt(math.pow((N_x - H_x), 2) +
                                    math.pow((N_y - H_y), 2) +
                                    math.pow((N_z - H_z), 2))

                vector_NH = numpy.array(
                    [(-N_x + H_x), (-N_y + H_y), (-N_z + H_z)])
                vector_HO = numpy.array(
                    [(O_x - H_x), (O_y - H_y), (O_z - H_z)])
                temp = math.sqrt(numpy.dot(vector_NH, vector_NH)) * \
                    math.sqrt(numpy.dot(vector_HO, vector_HO))
                cos_theta = numpy.dot(vector_NH, vector_HO) / temp

                if N_residue == H_residue:
                    if dist_OH < cutoff_hbond:
                        if cos_theta >= 0:
                            number_hbond += 1
                            hbond_strength += cos_theta / math.pow(dist_OH, 2)

        out_entry = N_residue + "," + \
            str(number_hbond) + "," + str(hbond_strength)
        list_amide_hbond_info.append(out_entry)

    return list_amide_hbond_info


def main(Assigned_PDBwH_folder):
    for subdir, dirs, files in os.walk(Assigned_PDBwH_folder):
        for file in files:
            file_path = subdir + file

            pdb = re.split("/", file_path)[-1]
            pdb = re.split(".pdb", pdb)[0]

            list1, list2, list3 = capture_c_beta(file_path)
            list_amide_hbond_info = hydrogen_calculation(list1, list2, list3)
            for element in list_amide_hbond_info:
                out = pdb + "," + element
                print(out)
    return 0


if __name__ == "__main__":
    main(Assigned_PDBwH_folder='/home/bwang/project/CancerSNP/main/calculation/Hbond/Div95ReduceAddH/')
