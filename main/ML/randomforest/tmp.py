#! /usr/bin/python3 
import re 

dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V",
}

dic_res_value = {}

feature_file = '/home/bwang/project/cancersnp/main/calculation/contact/AlphaEdge3layer'
f_fe = open(feature_file, 'r')
for line in f_fe:
    line = line.strip()
    pdb_id = re.split(',', line)[0]

    residue_info = re.split(',', line)[1]
    seq_num = re.split(':', residue_info)[1]
    res_tri = re.split(':', residue_info)[2]
    residue = dic_tri_single[res_tri]

    feature_value = ''
    for i in range(2, 61):
        feature_value += re.split(',', line)[i] + ','
    feature_value += re.split(',', line)[61]
    
    id = pdb_id + ',' + seq_num + ',' + residue
    id = id.upper()
    dic_res_value[id] = feature_value
f_fe.close()


file = 'Div80_Pos_Edge_Charge'
f = open(file, 'r')
for line in f:
    line = line.strip()
    if line != '':
        wild_aa = re.split(',', line)[3]    
        pdb_id = re.split(',', line)[4]
        seq_num = re.split(',', line)[5]

        if pdb_id.startswith('pdb') == 0:
            id = pdb_id + ',' + seq_num + ',' + wild_aa
            id = id.upper()
            try:
                value = dic_res_value[id]
                out_line = line + ',' + value
                print(out_line)
            except:
                pass
f.close()