#! /usr/bin/python3

import re

dic_Res_Charge = {
    "ALA": 0,
    "ARG": 1,
    "ASN": 0,
    "ASP": -1,
    "CYS": 0,
    "GLN": 0,
    "GLU": -1,
    "GLY": 0,
    "HIS": 1,
    "ILE": 0,
    "LEU": 0,
    "LYS": 1,
    "MET": 0,
    "PHE": 0,
    "PRO": 0,
    "SER": 0,
    "THR": 0,
    "TRP": 0,
    "TYR": 0,
    "VAL": 0,
}


dic_singler_tri = {
    "A": "ALA",
    "R": "ARG",
    "N": "ASN",
    "D": "ASP",
    "C": "CYS",
    "Q": "GLN",
    "E": "GLU",
    "G": "GLY",
    "H": "HIS",
    "I": "ILE",
    "L": "LEU",
    "K": "LYS",
    "M": "MET",
    "F": "PHE",
    "P": "PRO",
    "S": "SER",
    "T": "THR",
    "W": "TRP",
    "Y": "TYR",
    "V": "VAL",
}

f = open('Div80_Pos_wResVe', 'r')
for line in f:
    line = line.strip()
    wild_aa = re.split(',', line)[3]
    mutated_aa = re.split(',', line)[6]
    
    if line.startswith('label') == 0:
        wild = dic_singler_tri[wild_aa]
        mut = dic_singler_tri[mutated_aa]

        delta = dic_Res_Charge[wild] - dic_Res_Charge[mut]

        out_line = line + ',' + str(delta)
        print(out_line)

f.close()
