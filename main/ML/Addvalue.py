#! /usr/bin/python3
import re
import numpy as np 

dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V",
}

def ReadGeoSasa(GeoSasa_file):
    dic_residue_sasa = {}
    dic_residue_geo = {}

    f = open(GeoSasa_file, 'r')
    for line in f:
        line = line.strip()
        line_slices = re.split(',', line)

        pdb_id = line_slices[0].upper()
        pdb_seqnum = line_slices[2]
        wild_aa = line_slices[1]
        
        geo_location = line_slices[4]
        sasa = line_slices[5]

        entry_id = pdb_id + ',' + pdb_seqnum + ',' + wild_aa

        dic_residue_geo[entry_id] = geo_location
        dic_residue_sasa[entry_id] = sasa   

    f.close()
    return dic_residue_geo, dic_residue_sasa

def ReadDSSP(DSSP_file):
    dic_residue_SS = {}

    f = open(DSSP_file, 'r')
    for line in f:
        line = line.strip()
        line_slices = re.split(',', line)

        pdb_id = line_slices[0].upper()
        pdb_seqnum = line_slices[3]

        wild_aa = line_slices[1].upper()
        try:
            wild_aa = dic_tri_single[wild_aa]
            ss  = line_slices[4].upper()

            entry_id = pdb_id + ',' + pdb_seqnum + ',' + wild_aa
            dic_residue_SS[entry_id] = ss
        except:
            pass
    f.close()
    return dic_residue_SS

def AddValue(Dataset_file, DSSP):
    # dic_res_geo, dic_res_sasa = ReadGeoSasa(GeoSasa_file = GeoSasaFile)
    
    dic_res_ss = ReadDSSP(DSSP)

    f = open(Dataset_file, 'r')
    for line in f:
        line = line.strip()
        line_slices = re.split(',', line)

        up_id = line_slices[1]
        pdb_id = line_slices[4].upper()
        pdb_seqnum = line_slices[5]

        wild_aa = line_slices[3]
        mutated_aa = line_slices[6]

        entry_id = pdb_id + ',' + pdb_seqnum + ',' + wild_aa
        try:
            # out_line = line + dic_res_geo[entry_id] + ',' + dic_res_sasa[entry_id]
            out_line = line + ',' +dic_res_ss[entry_id]
            print(out_line)
        except:
            continue
    f.close()
    return 0 

AddValue(Dataset_file = 'Div95_B62_GSasa', 
         DSSP='/home/bwang/project/CancerSNP/main/ML/raw_feature_value/Div95.dssp')