#! /usr/bin/python3
import re
import numpy as np 

def Obtain_B_Score(aa_change_pattern):
    ## Given BLOSUM62 value
    ## https://www.ncbi.nlm.nih.gov/Class/FieldGuide/BLOSUM62.txt
    ## 23 by 23
    Matrix_Blosum62 = np.matrix(' 4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0 -4;\
                -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1 -4 ;\
                -2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1 -4 ;\
                -2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1 -4 ;\
                 0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2 -4 ;\
                -1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1 -4 ;\
                -1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 ;\
                 0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1 -4 ;\
                -2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1 -4 ;\
                -1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1 -4 ;\
                -1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1 -4 ;\
                -1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1 -4 ;\
                -1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1 -4 ;\
                -2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1 -4 ;\
                -1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2 -4 ;\
                 1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0 -4 ;\
                 0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0 -4 ;\
                -3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2 -4 ;\
                -2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1 -4 ;\
                 0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1 -4 ;\
                -2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1 -4 ;\
                -1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 ;\
                 0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1 -4 ;\
                -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  1 ')


    ## dictionary A.A. => Index in matrix 
    dic_aa_index = {'A': 0, 'R': 1, 'N': 2, 'D': 3, 'C': 4,
                    'Q': 5, 'E': 6, 'G': 7, 'H': 8, 'I': 9,
                    'L':10, 'K':11, 'M':12, 'F':13, 'P':14,
                    'S':15, 'T':16, 'W':17, 'Y':18, 'V':19,
                    'B':20, 'Z':21, 'X':22, '*':23 }

    first_char = aa_change_pattern[0]
    i = dic_aa_index[first_char]
    second_char = aa_change_pattern[1]
    j = dic_aa_index[second_char]

    score = Matrix_Blosum62[i,j]

    return score

def Addscore(Dataset_file):

    f = open(Dataset_file, 'r')
    for line in f:
        line = line.strip()
        line_slices = re.split(',', line)

        up_id = line_slices[1]
        pdb_id = line_slices[4]
        pdb_seqnum = line_slices[5]

        wild_aa = line_slices[3]
        mutated_aa = line_slices[6]

        aa_change = wild_aa + mutated_aa
        score = Obtain_B_Score(aa_change)
        
        out_line = line + str(score) + ','
        print(out_line)

    f.close()
    return 0 

Addscore('Div80')