#! /usr/bin/python3

import re 

## follow the PANTHER format to generate Mutation Info

def Reformat(mutation_file):

    f = open(mutation_file, 'r')
    i = 1
    for line in f:
        up_id = re.split(",", line)[1]
        seqnum = re.split(",", line)[2]
        wild_aa = re.split(",", line)[3]
        mutated_aa = re.split(",", line)[6]

        index = i 
        i += 1 
        out_entry = str(i) + "|" + up_id + "|" +\
                    seqnum + "|" + wild_aa + ";" + mutated_aa
        print(out_entry)
    f.close()

    return 0 

Reformat(mutation_file = '/home/bwang/project/cancersnp/main/ML/randomforest/Div80_Pos')