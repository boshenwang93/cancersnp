#! /usr/bin/python3

import re 

f = open('/home/bwang/project/cancersnp/main/ML/randomforest/Div80_Pos_Edge_Charge_EDContact', 'r')
for line in f:
    line = line.strip()
    uniprot_id = re.split(',', line)[1]
    seqnum = re.split(',', line)[2]
    wild_aa = re.split(',', line)[3]
    mut_aa = re.split(',', line)[6]

    if uniprot_id != 'uniprot':
        out_entry = uniprot_id + "\t" + seqnum + "\t" +\
                    wild_aa + "\t" + mut_aa
        print(out_entry)
f.close()