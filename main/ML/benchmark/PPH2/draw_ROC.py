#! /usr/bin/python3
import re
import matplotlib.pyplot as plt
import numpy as np

#######################################################
##########  ROC ####################################


list_pos_score = []
positive_ds = "Div80_Pos_Prob.txt"
f1 = open(positive_ds,"r")

for line in f1:
    line = line.strip()
    try:
        score = re.split("\t",line)[-3]
        score = float(score)
        if 0<= score <= 1:
            list_pos_score.append(score)
    except:
        pass
f1.close()

list_neg_score = []
negative_ds = "Div80_Neg_Prob.txt"
f2 = open(negative_ds,"r")
for line in f2:
    line = line.strip()
    try:    
        score = re.split("\t",line)[-3]
        score = float(score)
        if 0<=score<=1:
            list_neg_score.append(score)
    except:
        pass
f2.close()

count_postive = min( len(list_neg_score), len(list_pos_score))
count_negative = min( len(list_neg_score), len(list_pos_score))

list_neg_score = np.random.choice(list_neg_score, count_negative, replace=False)
list_pos_score = np.random.choice(list_pos_score, count_postive, replace=False)

######  ROC curve
list_sensitivity = []
list_fpr = []

i = 0
while i <= 1:
    i += 0.001
    count_tp = 0 
    for p in list_pos_score:
        if p > i:
            count_tp += 1
    
    count_tn = 0
    for n in list_neg_score:
        if n < i:
            count_tn += 1
    
    sensititivity = count_tp / count_postive
    fpr = 1 - count_tn / count_negative

    sensititivity = "%.3f"% sensititivity
    fpr  = "%.3f"% fpr

    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)

    list_sensitivity.append(sensititivity)
    list_fpr.append(fpr)


plt.plot(list_fpr, list_sensitivity, color = "red",   linewidth = 1 )

plt.plot([0,1],[0,1], "--", color = "black",  linewidth=1)

plt.axes().set_aspect('equal')
plt.show()


def auc(list_pos_score, list_neg_score, positive_label_count, negative_label_count):
    list_sensitivity = []
    list_fpr = []

    ######  ROC curve
    i = 0
    while i <= 1:
        i += 0.001
        count_tp = 0 
        for p in list_pos_score:
            if p > i:
                count_tp += 1
    
        count_tn = 0
        for n in list_neg_score:
            if n < i:
                count_tn += 1
    
        sensititivity = count_tp / positive_label_count
        fpr = 1 - count_tn / negative_label_count
        sensititivity = "%.3f"% sensititivity
        fpr  = "%.3f"% fpr
    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)
        list_sensitivity.append(sensititivity)
        list_fpr.append(fpr)

    auc = 0.0 
    for j in range(0,1000,1):
        dx =  -float(list_fpr[j+1]) + float(list_fpr[j]) 
        dy = (float(list_sensitivity[j+1]) + float(list_sensitivity[j]))/2
        area = dx * dy 
        auc += area
    # print("%.3f"% auc)

    return auc, list_sensitivity, list_fpr

auc, list_sen, list_fpr = auc(list_pos_score, list_neg_score, count_negative, count_postive)
print(auc)