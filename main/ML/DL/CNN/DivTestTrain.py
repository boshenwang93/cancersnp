#! /usr/bin/python3

import re 
import numpy as np

list_neg = []
list_pos = [] 

f = open('input','r')
for line in f:
    line = line.strip()
    label = re.split(',', line)[0]
    if int( label) == 0:
        list_neg.append(line)
    elif int(label) == 1:
        list_pos.append(line)
f.close()

list_test_neg = np.random.choice(list_neg, 200, replace=False)
for e in list_neg:
    if e in list_test_neg:
        list_neg.remove(e)
list_test_pos = np.random.choice(list_pos, 200, replace=False)
for e in list_pos:
    if e in list_test_pos:
        list_pos.remove(e)

f_test = open('test', 'w')
for e in list_test_neg:
    out_entry = e + "\n"
    f_test.write(out_entry)
for e in list_test_pos:
    out_entry = e + "\n"
    f_test.write(out_entry)
f_test.close()

f_train = open('train', 'w')
for e in list_neg:
    out_entry = e + "\n"
    f_train.write(out_entry)
for e in list_pos:
    out_entry = e + "\n"
    f_train.write(out_entry)
f_train.close()