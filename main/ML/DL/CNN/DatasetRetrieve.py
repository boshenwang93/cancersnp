#! /usr/bin/python3

import re

dic_single_tri = {
    "A": "ALA",
    "R": "ARG",
    "N": "ASN",
    "D": "ASP",
    "C": "CYS",
    "Q": "GLN",
    "E": "GLU",
    "G": "GLY",
    "H": "HIS",
    "I": "ILE",
    "L": "LEU",
    "K": "LYS",
    "M": "MET",
    "F": "PHE",
    "P": "PRO",
    "S": "SER",
    "T": "THR",
    "W": "TRP",
    "Y": "TYR",
    "V": "VAL"
}


list_neg = []
list_pos = []
list_pdb = []

raw_div80_neg = '/home/bwang/project/cancersnp/main/ML/randomforest/Div80_Neg'
raw_div80_pos = '/home/bwang/project/cancersnp/main/ML/randomforest/Div80_Pos'

f_neg = open(raw_div80_neg, 'r')
for line in f_neg:
    if line.startswith("label") == 0:
        pieces = re.split(",", line)
        label = pieces[0]
        wild_aa = pieces[3].upper()
        AA = dic_single_tri[wild_aa]
        pdb_id = pieces[4]
        seq_num = pieces[5]

        if pdb_id not in list_pdb:
            list_pdb.append(pdb_id)

        res_entry = label + ',' + pdb_id + ',' + seq_num + ',' + AA
        if res_entry not in list_neg:
            list_neg.append(res_entry)

f_neg.close()

f_pos = open(raw_div80_pos, 'r')
for line in f_pos:
    if line.startswith("label") == 0:

        pieces = re.split(",", line)
        label = pieces[0]
        wild_aa = pieces[3].upper()
        AA = dic_single_tri[wild_aa]
        pdb_id = pieces[4]
        seq_num = pieces[5]
 
        if pdb_id not in list_pdb:
            list_pdb.append(pdb_id)

        res_entry = label + ',' + pdb_id + ',' + seq_num + ',' + AA
        if res_entry not in list_pos:
            list_pos.append(res_entry)
f_pos.close()

for e in list_neg:
    if e in list_neg:
        list_neg.remove(e)
for e in list_pos:
    if e in list_neg:
        list_pos.remove(e)

out_dir = '/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/label/'
for each_pdb in list_pdb:
    new_file = out_dir + each_pdb + '.res'
    
    out = open(new_file, 'w')

    for res in list_neg:
        res_pdb = re.split(",", res)[1]
        if res_pdb == each_pdb:
            out_entry = res + "\n"
            out.write(out_entry)
    for res in list_pos:
        res_pdb = re.split(",", res)[1]
        if res_pdb == each_pdb:
            out_entry = res + "\n"
            out.write(out_entry)
    
    out.close()
