#! /usr/bin/python3

import os 
import re 

list_pixel_entry = []
pixel_dir = '/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/pixel/'
for root, dirs, files in os.walk(pixel_dir):
    for file in files:
        file_path = root + file
        if file.endswith(".pixel"):
            f = open(file_path, 'r')
            for line in f:
                line = line.strip()
                list_pixel_entry.append(line)
            f.close()

list_label = []
label_dir = '/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/label/'
for root, dirs, files in os.walk(label_dir):
    for file in files:
        file_path = root + file
        if file.endswith(".res"):
            f = open(file_path, 'r')
            for line in f:
                line = line.strip()
                list_label.append(line)
            f.close()

for each_pixel in list_pixel_entry:
    pcs_pixel = re.split(",", each_pixel)
    pixel_id = pcs_pixel[0] + ',' + pcs_pixel[2] + ',' + pcs_pixel[3]

    for each_label in list_label:
        pcs_label = re.split(',', each_label)
        label_id = pcs_label[1] +',' + pcs_label[2] + ','+pcs_label[3]
        label = pcs_label[0]

        if pixel_id == label_id:
            out_entry = label + ','+ each_pixel
            print(out_entry)
