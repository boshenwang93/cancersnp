#! /usr/bin/python3 

import re 
import os 

list_pdb_ae = []
for root, dirs, files in os.walk('/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/raw/'):
    for file in files:
        if file.endswith("alpha.edges"):
            pdb = re.split("\.alpha.edges", file)[0]
            list_pdb_ae.append(pdb)


list_pdb_label = []
for root, dirs, files in os.walk('/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/label/'):
    for file in files:
        if file.endswith("res"):
            pdb = re.split("\.res", file)[0]
            list_pdb_label.append(pdb)

for e in list_pdb_ae:
    if e not in list_pdb_label:
        print(e)