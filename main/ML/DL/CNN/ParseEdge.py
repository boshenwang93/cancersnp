#! /usr/bin/python3 

import re
import os 
from multiprocessing import Process, Pool


def GeneratePixelImageFrame():

    dic_residue_heavyatom = {
        "ALA": ("N", "CA", "C", "O", "CB"),
        "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
        "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
        "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
        "CYS": ("N", "CA", "C", "O", "CB", "SG"),
        "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
        "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
        "GLY": ("N", "CA", "C", "O"),
        "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
        "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
        "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
        "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
        "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
        "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
        "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
        "SER": ("N", "CA", "C", "O", "CB", "OG"),
        "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
        "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
        "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
        "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
     }

    list_atom = []
    for k,v in dic_residue_heavyatom.items():
        for each_element in v:
            if each_element not in list_atom:
                list_atom.append(each_element)
    list_atom.sort()
    
    list_atom_contact_pair = []
    for i in range(0, len(list_atom)):
        for j in range(0, len(list_atom)):
            pair = list_atom[i] + ',' + list_atom[j]
            list_atom_contact_pair.append(pair)

    return list_atom_contact_pair


def GetAlphaEdge(AlphaEdgeFile):

    list_residue = []
    list_alpha_edge_pair = []

    f = open(AlphaEdgeFile, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            list_line_slices = re.split(' ', line)
            atom1 = list_line_slices[0]
            atom2 = list_line_slices[1]
            
            list_s_atom1 = re.split(':', atom1)
            atom1_pm = list_s_atom1[0] + ',' +\
                       list_s_atom1[1] + ',' +\
                       list_s_atom1[2] + ',' +\
                       list_s_atom1[4]

            atom1_res =list_s_atom1[0] + ',' +\
                       list_s_atom1[1] + ',' +\
                       list_s_atom1[2] 
                       
            list_s_atom2 = re.split(':', atom2)
            atom2_pm = list_s_atom2[0] + ',' +\
                       list_s_atom2[1] + ',' +\
                       list_s_atom2[2] + ',' +\
                       list_s_atom2[4]
            atom2_res = list_s_atom2[0] + ',' +\
                       list_s_atom2[1] + ',' +\
                       list_s_atom2[2] 

            if atom1_res not in list_residue:
                list_residue.append(atom1_res)
            if atom2_res not in list_residue:
                list_residue.append(atom2_res)

            entry = atom1_pm + ',' + atom2_pm
            list_alpha_edge_pair.append(entry)
    f.close()

    list_residue.sort()

    return list_alpha_edge_pair


def GenerateVector(Residue_list, Alpha_edge_list, All_atom_contact_list, 
                   PDB_ID, out_file_path):
    
    out = open(out_file_path, 'w')

    for i in range(0, len(Residue_list)):
        each_residue = Residue_list[i]

        out_entry = PDB_ID + ',' + each_residue + ','
        total_count = 0

        for j in range(0, len(All_atom_contact_list)):
            each_atom_contact_pair = All_atom_contact_list[j]
            count = 0 

            for k in range(0, len(Alpha_edge_list) ):
                each_alpha_edge = Alpha_edge_list[k]

                pieces = re.split(",", each_alpha_edge)

                alpha_edge_res_id_1 = pieces[0] + ',' + pieces[1] + ',' + pieces[2]
                alpha_edge_atom_1 = pieces[3]

                alpha_edge_res_id_2 = pieces[4] + ',' + pieces[5] + ',' +pieces[6]
                alpha_edge_atom_2 = pieces[7]

                alpha_edge_pair_1 = alpha_edge_atom_1 + ',' + alpha_edge_atom_2
                alpha_edge_pair_2 = alpha_edge_atom_2 + ',' + alpha_edge_atom_1

                if alpha_edge_res_id_1 == each_residue:
                    if alpha_edge_pair_1 == each_atom_contact_pair:
                        count += 1
                if alpha_edge_res_id_2 == each_residue:
                    if alpha_edge_pair_2 == each_atom_contact_pair:
                        count += 1
            out_entry +=  str(count) + ','
            total_count += count
        
        out_entry += "\n"
        out.write(out_entry)
        ## for checking
        # print(each_residue, str(total_count))
    out.close()
    return 0

def RetrieveInterestRes(Residue_file):
    list_res_interest = []

    f = open(Residue_file, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            list_res_interest.append(line)
    f.close()

    return list_res_interest

def MergeTwoSteps(raw_alpha_edge_path):
    list_ae_pair = GetAlphaEdge(AlphaEdgeFile = raw_alpha_edge_path)
    
    pdb_name = re.split("\/", raw_alpha_edge_path)[-1]
    pdb_name = re.split('.alpha.edges', pdb_name)[0].strip()
    
    chain = re.split("_", pdb_name)[-1]
    residue_dir = '/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/label/'
    residue_f = residue_dir + pdb_name + '.res'
    list_interest_res = RetrieveInterestRes(Residue_file = residue_f)
    
    list_res = []
    for e in list_interest_res:
        res_entry = chain + ',' + re.split(',', e)[2] +',' +\
                    re.split(',', e)[3]
        list_res.append(res_entry)

    new_file = '/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/pixel/' + pdb_name + '.pixel'

    GenerateVector(Residue_list = list_res,
               Alpha_edge_list = list_ae_pair,
               All_atom_contact_list = GeneratePixelImageFrame(),
               PDB_ID = pdb_name,
               out_file_path = new_file )
    
    return 0 

def main(assigned_folder):
    # iterating the pdb folder
    list_alpha_edge_file = []
    pdb_directory = assigned_folder
    for root, dirs, files in os.walk(pdb_directory):
        for file in files:
            file_path = root + file
            if file.endswith("alpha.edges"):
                list_alpha_edge_file.append(file_path)
    return list_alpha_edge_file

if __name__ == "__main__":
    list_alpha_edge_files = main(assigned_folder = '/home/bwang/project/cancersnp/main/ML/DL/CNN/AlphaEdge/raw/')
    
    with Pool(processes= 10) as pool:
        print(pool.map(MergeTwoSteps, list_alpha_edge_files))
    