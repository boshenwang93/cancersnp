#! /usr/bin/python3

import os
import re 

def RetrievePDB(Sites_file):
    list_NR_pdb = []

    f = open(Sites_file)
    for line in f:
        if re.search(',', line):
            list_slices = re.split(',', line)
            pdb_chain = list_slices[3].strip().upper()
            pdb = re.split(':', pdb_chain)[0]

            if pdb not in list_NR_pdb:
                list_NR_pdb.append(pdb)
    f.close()

    return list_NR_pdb 


def DownloadPdb(pdb, download_dir):
    rcsb_link = "https://files.rcsb.org/download/" + pdb + ".pdb"
    command_text = "wget " + rcsb_link + " -P " + download_dir 
    os.system(command_text)
    return 0 

def main():

    file_path  = '/home/bwang/project/cancersnp/data/pph/DivPDB95.sites'
    list_NR_pdb = RetrievePDB(Sites_file = file_path)
    for e in list_NR_pdb:
        DownloadPdb(pdb = e,
                    download_dir = '/home/bwang/project/cancersnp/data/pph/pdb/raw/')
    return 0 

if __name__ == '__main__':
    main()