#! /usr/bin/python3

import os
import re 

def RetrievePDB(Sites_file):
    list_NR_pdb = []
    f = open(Sites_file)
    for line in f:
        if re.search(',', line):
            list_slices = re.split(',', line)
            pdb_w_chain = list_slices[3].strip().upper()
            pdb_w_chain = re.sub(':', '_', pdb_w_chain)
            if pdb_w_chain not in list_NR_pdb:
                list_NR_pdb.append(pdb_w_chain)
    f.close()

    return list_NR_pdb

def main():

    file_path  = '/home/bwang/project/CancerSNP/data/pph/DivPDB95.sites'
    out_dir = '/home/bwang/project/CancerSNP/data/pph/pdb/DivPdb95/'

    list_NR_pdb = RetrievePDB(Sites_file = file_path)
    pdb_singlechain_dir = '/home/bwang/project/CancerSNP/data/pph/pdb/single_conform_chain/'

    for e in list_NR_pdb:
        in_file_path = pdb_singlechain_dir + e + '.pdb'
        command = 'cp ' + in_file_path + "\t" + out_dir
        os.system(command)

    return 0 

if __name__ == '__main__':
    main()