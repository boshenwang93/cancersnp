#! /usr/bin/python3
import os 
import re

###########################################################
#### retrieve PDB seq info to fasta format ################
#### PDB seq info may differ fasta provided by RCSB #######
#### Written by B.Wang,  Feb 2018 #########################
###########################################################

def PDB2fasta(pdb_file_path, out_fasta_file):
    ## open the file hander for out 
    out = open(out_fasta_file, 'a')

    # dictionary Residue Tri_Letter => One Letter
    dic_tri_single = {
        "ALA": "A",
        "ARG": "R",
        "ASN": "N",
        "ASP": "D",
        "CYS": "C",
        "GLN": "Q",
        "GLU": "E",
        "GLY": "G",
        "HIS": "H",
        "ILE": "I",
        "LEU": "L",
        "LYS": "K",
        "MET": "M",
        "PHE": "F",
        "PRO": "P",
        "SER": "S",
        "THR": "T",
        "TRP": "W",
        "TYR": "Y",
        "VAL": "V"
    }

    # initalize list for chain, dictionary for chain => start seqnum, end seqnum 
    # dic_end ['A'] = 50 
    list_chain = []
    dic_chain_start =  {}
    dic_chain_end = {}
    PDB = ''
    
    ## obtain list of chain, PDB ID
    f = open(pdb_file_path, 'r')
    for line in f:
        if line.startswith("HEADER"):
                PDB += line[62:66]
        if line.startswith("ATOM"):
            # residue name
            residue_tri = ""
            for i in range(17, 20, 1):
                residue_tri += line[i]
            residue_tri = residue_tri.strip()
            residue_tri = residue_tri.upper()

            # chain identifier
            chain = line[21]

            # push unique chain to list
            if chain not in list_chain:
                list_chain.append(chain)
    f.close()
    PDB = PDB.strip()

    ## obtain dictionary for Chain => start/end seqnum 
    for each_chain in list_chain:
        list_seqnum = [] 
        f = open(pdb_file_path, 'r')
        for line in f:        
            if line.startswith("ATOM"):
                # chain identifier
                chain = line[21]

                # residue sequence number
                residue_seq_number = ""
                for i in range(22, 26, 1):
                    residue_seq_number += line[i]
                residue_seq_number = residue_seq_number.strip()

                if chain == each_chain:
                    residue_seq_number = int(residue_seq_number)
                    list_seqnum.append(residue_seq_number)
        f.close()
        dic_chain_start[each_chain] = min(list_seqnum)
        dic_chain_end[each_chain] = max(list_seqnum)
    

##### Complete chain list, diction_start/end[chain] = seqnum 

#################################################################
    for each_chain in list_chain:

        ## the header info in for the seq, including PDB start/end seqnum  
        header = '>' + PDB + ':' + each_chain + ',' +\
                 'start:' + str(dic_chain_start[each_chain]) + ',' +\
                 'end:' + str(dic_chain_end[each_chain])

        ## initialize the sequence string, as 'X' * length
        seq_length = dic_chain_end[each_chain] - dic_chain_start[each_chain] + 1  
        seq_string = 'X' * seq_length

        f = open(pdb_file_path, 'r')
        for line in f:
            if line.startswith("ATOM"):
                # residue triple letter 
                residue_tri = ""
                for i in range(17, 20, 1):
                    residue_tri += line[i]
                residue_tri = residue_tri.strip()
                residue_tri = residue_tri.upper()

                residue_one = dic_tri_single[residue_tri]

                # chain identifier
                chain = line[21]

                # residue sequence number
                residue_seq_number = ""
                for i in range(22, 26, 1):
                    residue_seq_number += line[i]
                residue_seq_number = residue_seq_number.strip()

                index_in_seq = int(residue_seq_number) - dic_chain_start[each_chain]

                if chain == each_chain:
                    seq_string = seq_string[:index_in_seq] + residue_one + seq_string[index_in_seq+1:]
        f.close()
        entry_string = header + "\n" + seq_string + "\n"
        out.write(entry_string)
    
    out.close()
    return 0

# sample_PDB_file = "/home/bwang/project/cancersnp/test.pdb"
# PDB2fasta(pdb_file_path = sample_PDB_file,
#           out_fasta_file = 'test.fasta')

def main():
    ## assign root folder 
    PDB_folder = '/home/bwang/PDB/'
    ## obtain the paths for divided folder 
    list_dirs = [] 
    for root, dirs, files in os.walk(PDB_folder):
        for subdir in dirs:
            subdir_abs_path = root + subdir + '/'
            list_dirs.append(subdir_abs_path)
    
    ## unzip all .gz files 
    for each_subdir in list_dirs:
        for root, dirs, files in os.walk(each_subdir):
            print(each_subdir)
            for each_file in files:
                file_abs_path = each_subdir + each_file
                if file_abs_path.endswith('.gz'):
                    cmd = 'gzip -d ' + file_abs_path
                    os.system(cmd)

    ## Collect the fasta seq info 
    log = open('/home/bwang/PDBfasta/convert.log', 'a')
    for each_subdir in list_dirs:
        for root, dirs, files in os.walk(each_subdir):
            for each_file in files:
                if each_file.startswith('pdb') == 1:  
                    file_abs_path = each_subdir + each_file
                    try:
                        PDB2fasta(pdb_file_path = file_abs_path,
                                  out_fasta_file = '/home/bwang/PDBfasta/allPDB.fasta')
                    except:
                        out_entry = each_file + "\t" + 'has problem'
                        log.write(out_entry)
    log.close()
    
    return 0 

if __name__ == "__main__":
    main()