#! /usr/bin/python3
import re 
import os

## obtain pph mutation info
def RetrieveProteinInfo(Polyphen_Input_Sum_file_path):
    ## initialize the list for unqiue Mutation info
    list_mutation_info = [] 

    ## read the Polyphen Input Summary File 
    f = open(Polyphen_Input_Sum_file_path, 'r')
    for line in f:
        slices = re.split("\t", line)
        uniprot_id = slices[0].strip().upper()
        up_seqnum = slices[1].strip().upper()
        wild_type = slices[2].strip().upper()
        mutated_type = slices[3].strip().upper()

        entry = uniprot_id + ',' + up_seqnum + ',' +\
                wild_type + ',' + mutated_type
        list_mutation_info.append(entry)
    f.close()
    
    ## return the value needed
    return list_mutation_info



def GenerateDataset(Sites_file, out_file_path):
    list_positive_mutation = RetrieveProteinInfo(Polyphen_Input_Sum_file_path =\
        '/home/bwang/project/CancerSNP/data/pph/raw/humdiv-2011_12.deleterious.pph.input')
    list_negative_mutation = RetrieveProteinInfo(Polyphen_Input_Sum_file_path=\
        '/home/bwang/project/CancerSNP/data/pph/raw/humdiv-2011_12.neutral.pph.input')
    
    out = open(out_file_path, 'w')
    f = open(Sites_file)
    for line in f:
        line = line.strip()

        if re.search(',', line):
            list_slices = re.split(',', line)
            uniprot = list_slices[0].strip().upper()
            uniprot_seqnum = list_slices[1].strip().upper()
            uniprot_res = list_slices[2].strip().upper()

            site_id = uniprot + ',' + uniprot_seqnum + ',' + uniprot_res

            for element in list_negative_mutation:
                slices = re.split(',', element)
                element_id = slices[0] +',' + slices[1] +',' + slices[2]
                mutated_aa = slices[3]
                if element_id == site_id:
                    out_line = 'Negative,' + line + ',' + mutated_aa + "\n"
                    out.write(out_line)
            for element in list_positive_mutation:
                slices = re.split(',', element)
                element_id = slices[0] +',' + slices[1] +',' + slices[2]
                mutated_aa = slices[3]
                if element_id == site_id:
                    out_line = 'Positive,' + line + ',' + mutated_aa + "\n"
                    out.write(out_line)

    f.close()
    out.close()

    return 0

GenerateDataset(Sites_file = '/home/bwang/project/CancerSNP/data/pph/DivPDB80.sites',
                out_file_path = 'DatasetDiv80' )