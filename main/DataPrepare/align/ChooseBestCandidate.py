#! /usr/bin/python3 

import re 

###########################################################
#### Choose the best two-way identity candidate ###########
###########################################################

## input as the one-way rawly filtered identity summary file
## raw filter as one-way identity >= 80%
def ChooseBestCandidate(raw_filtered_identity_sum_file):

    ## obtain the full list for unique uniprot ids
    list_unique_uniprot = [] 
    f = open(raw_filtered_identity_sum_file, 'r')
    for line in f:
        if line.startswith(">"):
            
            uniprot_in_line = line[1:7]
            if uniprot_in_line not in list_unique_uniprot:
                list_unique_uniprot.append(uniprot_in_line)
    f.close()
    
    ## Obtain the best candidate pairs 
    for each_uniprot in list_unique_uniprot:

        ## obtain the highest Two-way identity score 
        list_score = []
        f = open(raw_filtered_identity_sum_file, 'r')
        for line in f:            
            uniprot_in_line = line[1:7]

            if uniprot_in_line == each_uniprot:
                identity_against_uniprot = float(re.split(',', line)[-2])
                identity_against_pdb = float(re.split(',', line)[-1])
  
                score = identity_against_uniprot + identity_against_pdb
                list_score.append(score)
        f.close()

        max_score_twoway_identity = max(list_score)

        f = open(raw_filtered_identity_sum_file, 'r')
        for line in f:            
            uniprot_in_line = line[1:7]
            if uniprot_in_line == each_uniprot:
                identity_against_uniprot = float(re.split(',', line)[-2])
                identity_against_pdb = float(re.split(',', line)[-1])
  
                score = identity_against_uniprot + identity_against_pdb
                if score == max_score_twoway_identity:
                    print(line,end='')
        f.close()
    

    return 0 

ChooseBestCandidate(raw_filtered_identity_sum_file = 'Identity_info')