#! /usr/bin/python3

import re
import random 

def SinglePair(UniprotPDB_iden_pair_file):
    ## initialize dictionary uniprot=> one pdb chain
    dic_uniprot_pdb = {}
    
    ## get non-redundant uniprot list
    list_NR_uniprot = []
    f = open(UniprotPDB_iden_pair_file, 'r')
    for line in f:
        slices = re.split(',', line)
        uniprot_id = slices[0]
        if uniprot_id not in list_NR_uniprot:
            list_NR_uniprot.append(uniprot_id)
    f.close()

    ## randomly choose a chain => uniprot pair
    for each_uniprot in list_NR_uniprot:
        list_best_pdb = []
        f = open(UniprotPDB_iden_pair_file, 'r')
        for line in f:
            slices = re.split(',', line)
            uniprot_id = slices[0]
            pdb_chain = slices[1] + ',' + slices[2] + ',' + slices[3]
            if uniprot_id == each_uniprot:
                list_best_pdb.append(pdb_chain)             
        f.close()
        one_pdb = random.choice(list_best_pdb)
        dic_uniprot_pdb[each_uniprot] = one_pdb

    return dic_uniprot_pdb, list_NR_uniprot

def RetrievePPH(pphDS, dictionary_uniprot_pdb, 
                list_NonRedundant_Uniprot,
                residue_mapping_info_dir):

    ## Retrieve dictionary uniprot => variant(seqnum + residue )
    dic_uniprot_variantlist = {}
    for each_uniprot in list_NonRedundant_Uniprot:
        list_aa_info = []

        ## read pph dataset 
        f_pph = open(pphDS, 'r')
        for line in f_pph:
            slices = re.split("\t", line)
            uniprot_id = slices[0].strip().upper()
            seqnum = slices[1].strip()
            wild_type = slices[2].strip()
            mutated_type = slices[3].strip()

            aa_info = seqnum + ',' + wild_type
            if uniprot_id == each_uniprot:
                if aa_info not in list_aa_info:
                    list_aa_info.append(aa_info)
                # list_aa_info.append(aa_info)
        f_pph.close()
        dic_uniprot_variantlist[each_uniprot] = list_aa_info
    
    ## obtain mapping information
    dic_uniprot_aa_w_map = {}
    dic_uniprot_mapping_info = {}
    for each_uniprot in list_NonRedundant_Uniprot:
        list_aa_info = []

        mapping_dir = residue_mapping_info_dir
        mapping_file_name = each_uniprot + '_' +\
                            dictionary_uniprot_pdb[each_uniprot] + '.mapping'
        mapping_file_path = mapping_dir + mapping_file_name

        f = open(mapping_file_path, 'r')
        for line in f:
            list_line_slices = re.split(',', line)
            pdb_seq_num = list_line_slices[-1].strip()
            uniprot_index = list_line_slices[-2].strip()
            residue = list_line_slices[-3]
            aa_info = uniprot_index + ',' + residue
            list_aa_info.append(aa_info)

            mapping_info = each_uniprot + ',' +\
                           uniprot_index + ',' + residue
            dic_uniprot_mapping_info[mapping_info] = pdb_seq_num
        f.close()
        dic_uniprot_aa_w_map[each_uniprot] = list_aa_info
    
    ## Generate DS with known structure mapping sides
    list_count_protein = []
    for k,v in dic_uniprot_variantlist.items():
        up_id = k
        list_snps = v

        for each_snp in list_snps:
            map_key = up_id + ',' + each_snp
            try:
                pdb_index = dic_uniprot_mapping_info[map_key]
                out_entry = map_key + ',' + dictionary_uniprot_pdb[up_id] + ',' + pdb_index
                ## for check , 
                ## print the sites (ignore mutation pattern)
                print(out_entry)

                if up_id not in list_count_protein:
                    list_count_protein.append(up_id)
            except:
                pass
    # print(len(list_count_protein))
    return  dic_uniprot_variantlist

def main():
    dic_uniprot_pdb, list_NR_up = SinglePair(UniprotPDB_iden_pair_file =\
                                  '/home/bwang/project/cancersnp/data/pph/PairIden80')

    d1 = RetrievePPH(pphDS = '/home/bwang/project/cancersnp/data/pph/DSDiv',
                dictionary_uniprot_pdb = dic_uniprot_pdb,
                list_NonRedundant_Uniprot = list_NR_up,
                residue_mapping_info_dir = '/home/bwang/project/cancersnp/calculation/alignment/mapping/' )
    
    # for k,v in d1.items():
    #     if v  != []:
    #         for element in v:
    #             print(k, element)

    return 0 

if __name__ == "__main__":
    main()