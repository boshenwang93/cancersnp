#! /usr/bin/python3 

import re
import os 

###########################################################
#### From One2One to achieve Residue mapping  #############
###########################################################

## input as the best identity pairs 
## randomly choose one PDB chain
def RetrieveResidueMapping(One2One_sum_file):

    ## obtain the paired best One2One Prefix
    list_file_prefix = [] 

    f = open(One2One_sum_file, 'r')
    for line in f:
        uniprot_in_line = re.split(',', line)[0].strip()
        pdb_chain = re.split(',', line)[1].strip()

        file_prefix = uniprot_in_line + '_' + pdb_chain
        list_file_prefix.append(file_prefix)
    f.close()

    ## walk in the mapping folder 
    for root, subdirs, files in os.walk('/home/bwang/ResidueMapping/'):
        for each_file in files:
            each_file_path = root + each_file

            each_file_prefix = each_file[0:13]
            if each_file_prefix in list_file_prefix:
                print(each_file_path)
                copy_cmd = 'cp ' + each_file_path + ' ' +\
                           '/home/bwang/project/cancersnp/data/polyphen2/Div/mapping/'
                os.system(copy_cmd)
    return 0 

RetrieveResidueMapping(One2One_sum_file = 'One2One')