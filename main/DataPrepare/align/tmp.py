#! /usr/bin/python3

import re
import os

list_unique = []
for root, dirs, files in os.walk('/home/bwang/project/cancersnp/data/polyphen2/RawAlignedFasta/'):
    for each_file in files:
        uniprot_id = each_file[0:6]
        if uniprot_id not in list_unique:
            list_unique.append(uniprot_id)

# print(len(list_unique))

list_unique_uniprot = []
f = open('Identity_info', 'r')
for line in f:
    uniprot = line[1:7]
    if uniprot not in list_unique_uniprot:
        list_unique_uniprot.append(uniprot)
f.close()

print(len(list_unique_uniprot))