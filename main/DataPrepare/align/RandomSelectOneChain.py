#! /usr/bin/python3 

import re
import random 

###########################################################
#### Randomly Select One Chain from Candidates  ###########
###########################################################

## input as the best identity pairs 
## randomly choose one PDB chain
def RandomChooseOneChain(Best_candidate_sum_file):

    ## obtain the full list for unique uniprot ids
    list_unique_uniprot = [] 
    f = open(Best_candidate_sum_file, 'r')
    for line in f:
        if line.startswith(">"):
            
            uniprot_in_line = line[1:7]
            if uniprot_in_line not in list_unique_uniprot:
                list_unique_uniprot.append(uniprot_in_line)
    f.close()

    ## obtain the list of candidates belonging to each_uniprot
    for each_uniprot in list_unique_uniprot:
        list_candidates = [] 

        f = open(Best_candidate_sum_file, 'r')
        for line in f:
            uniprot_in_line = line[1:7]

            pdb_chain = re.split(',', line)[1]

            if uniprot_in_line == each_uniprot:
                list_candidates.append(pdb_chain)
        f.close()

        selection = random.choice(list_candidates)
        
        out_line = each_uniprot + ',' + selection
        print(out_line)

    return 0 

RandomChooseOneChain( Best_candidate_sum_file = 'BestCandidate')