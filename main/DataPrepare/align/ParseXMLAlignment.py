#! /usr/bin/python3 

import xml.etree.ElementTree as ET 
import os 
import re 

#######################################
#### Parse BLAST XML Out ##############
#### Select best hits #################
#######################################
def Retrieve_Uniprot_Seq(uniprot_fasta_file_path):
    seq_content = ''
    with open(uniprot_fasta_file_path, 'r') as f:
        seq_content += f.read()
    f.close()
    return seq_content

def Retrieve_pdb_seq(pdb_chain_id_w_seqnum, all_pdb_fasta_path):
    seq_content = ">"
    
    with open(all_pdb_fasta_path, 'r') as f:
        f_content = f.read()
        list_seg = re.split(">", f_content)
        
        for each_seg in list_seg:
            if each_seg.startswith(pdb_chain_id_w_seqnum):
                seq_content += each_seg
    f.close()
    return seq_content


def ChooseHits(BLAST_XML_Output_file):

    ## initialize dictionary(uniprot=> list hits) 
    list_hits = []
    uniprot_id = ''

    ## read the XML file into a ET parser
    tree = ET.parse(BLAST_XML_Output_file)
    root = tree.getroot()

    ## find hits 
    for first_layer in root:    
        if 'BlastOutput_query-def' == first_layer.tag:
            query_info = first_layer.text
            uniprot_id += query_info.upper()
            uniprot_id = uniprot_id.strip().upper()
            
            ## uniprot fasta file path 
            ## !!!!!!!!!!!! @@@@@@@@@@
            uniprot_fasta_dir = '/home/bwang/project/cancersnp/data/polyphen2/Div/fasta/'
            uniprot_fasta_abs_path = uniprot_fasta_dir + uniprot_id + '.fasta'
            ## fasta format for uniprot seq
            uniprot_seq_fasta = Retrieve_Uniprot_Seq(uniprot_fasta_file_path = uniprot_fasta_abs_path)
            Seq_slices = re.split("\n", uniprot_seq_fasta)
            Seq_string = ''
            for each_part in Seq_slices:
                if each_part.startswith(">") == 0:
                    Seq_string += each_part

            ## length for uniprot seq length 
            seq_length = len(Seq_string)

        ##find the iterations segments
        elif 'BlastOutput_iterations' == first_layer.tag:
            for second_layer in first_layer:
                if 'Iteration' == second_layer.tag:
                    for third_layer in second_layer:
                        if 'Iteration_hits' == third_layer.tag:
                            for fourth_layer in third_layer:
                                if 'Hit' == fourth_layer.tag:
                                    pdb_chain = ''

                                    for fifth_layer in fourth_layer:
                                        if 'Hit_def' == fifth_layer.tag:
                                            pdb_chain += fifth_layer.text
                                            
                                    list_hits.append(pdb_chain)

    return uniprot_id, list_hits 


def main(): 
    ## dic for uniprot => list best hits
    dic_uniprot_besthits = {}

    ##BLAST XML output path 
    BLAST_XML_dir = '/home/bwang/project/cancersnp/data/polyphen2/BlastCandidate/'
    
    ## uniprot fasta file path 
    uniprot_fasta_dir = '/home/bwang/project/cancersnp/data/polyphen2/Div/fasta/'

    ## BLAST all fasta in folder 
    for root, dirs, files in os.walk(BLAST_XML_dir):
        for single_file in files:
            file_asb_path = BLAST_XML_dir + single_file
            if single_file.endswith('.XML'):
                uniprot, list_hits = ChooseHits(BLAST_XML_Output_file = file_asb_path)

                ## print out the Uniprot => PDB candidate
                if list_hits != []:
                    out_entry_line = uniprot + "\t"
                    for each_pdb_hit in list_hits:
                        out_entry_line += each_pdb_hit + "\t"
                    out_entry_line += "\n"

                    print(out_entry_line)

    return 0 

if __name__ == '__main__':
    main()