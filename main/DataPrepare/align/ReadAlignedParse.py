#! /usr/bin/python3

import re
import os 

#### Calculate identity and Residue-level mapping 
def ReadIdentity(alignedFasta_file, out_file_path, threshold_iden):
    uniprot_header =''
    pdb_header = ''

    with open(alignedFasta_file) as f:
        all_context = f.read()
    f.close()

    list_slices = re.split('>', all_context)

    seq1_w_header = '>' + list_slices[1]
    seq2_w_header = '>' + list_slices[2]

    list_lines_seq1 = re.split("\n", seq1_w_header)
    list_lines_seq2 = re.split("\n", seq2_w_header)
    
    seq1_context = ''
    seq2_context = ''

    for eachline in list_lines_seq1:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq1_context += eachline
        else:
            uniprot_header += eachline
                
    for eachline in list_lines_seq2:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq2_context += eachline
        else:
            pdb_header += eachline

    ##obtain pdb start seqnum 
    pdb_header_slices = re.split('_', pdb_header)
    begin_seq_num_pdb = int( pdb_header_slices[3])
    pdb_chain_id = pdb_header_slices[0] + ':' + pdb_header_slices[1] +',' +\
                   'start:' + pdb_header_slices[3] + ',' +\
                   'end:' + pdb_header_slices[5]
    pdb_chain_id = re.sub('>', '', pdb_chain_id)

    ## get uniprot
    uniprot_id = uniprot_header.upper()

    ## check length equal 
    m = len(seq1_context)
    n = len(seq2_context)

    ## count identity
    gaps_seq1 = 0 
    gaps_seq2 = 0 
    identity_chars = 0 
    for i in range(0, m ,1):
        if seq1_context[i] == seq2_context[i]:
            identity_chars += 1
        if seq1_context[i] == '-':
            gaps_seq1 += 1
        if seq2_context[i] == '-':
            gaps_seq2 += 1
    ## without gap, the length of sequence 
    seq1_wo_gap = m - gaps_seq1
    seq2_wo_gap = n - gaps_seq2
    ## calculate the identity of sequence
    seq1_identity = identity_chars / seq1_wo_gap
    seq2_identity = identity_chars / seq2_wo_gap
    
    ## Match info 
    matching_entry = ''
    for i in range(0, m, 1):
        ## for seq 1 
        ## count gaps before i+1 th string
        Seq1_gap_before = 0
        for j in range(0, i, 1):
            if seq1_context[j] == '-':
                Seq1_gap_before += 1 
        actual_index_uniprot = i - Seq1_gap_before + 1

        Seq2_gap_before = 0
        for j in range(0, i, 1):
            if seq2_context[j] == '-':
                Seq2_gap_before += 1 
        index_pdb = i - Seq2_gap_before + 1
        actual_index_pdb = index_pdb - 1 + begin_seq_num_pdb

        if seq1_context[i] != '-':
            if seq2_context[i] != '-':
                if seq1_context[i] == seq2_context[i]:
                    out_entry = uniprot_id + ',' +\
                                pdb_chain_id + ',' +\
                                str(seq1_identity) + ',' +\
                                seq1_context[i] + ',' +\
                                str(actual_index_uniprot) + ',' +\
                                str(actual_index_pdb) + "\n"
                    matching_entry += out_entry

    if seq1_identity >= threshold_iden:
        if seq2_identity >= threshold_iden:
            out = open(out_file_path, 'w')
            out.write(matching_entry)
            out.close()
    
    seq_identity_info = uniprot_id + ',' +\
                        pdb_chain_id + ',' +\
                        str(seq1_identity) + ',' +\
                        str(seq2_identity)
    return seq_identity_info, seq1_identity

#### read Candidate file 
def ReadCandidate(candiate_file):
    ## dictionary uniport => list candidate/best hits
    dic_uniport_candiate = {}

    f = open(candiate_file, 'r')
    for line in f:
        if re.search("\t", line):
            slices = re.split("\t", line)
            uniprot = slices[0].strip()
            list_NR_hits = []
            for i in range(1, len(slices)):
                pdb_chain = slices[i].strip()
                if pdb_chain not in list_NR_hits:
                    if pdb_chain != '':
                        list_NR_hits.append(pdb_chain)
            
            dic_uniport_candiate[uniprot] = list_NR_hits
    f.close()

    return dic_uniport_candiate


def main(candidate_file, threshold_iden_score):
    ## get dictionary uniprot => candidate
    dic_uniprot_candidate = ReadCandidate(candidate_file)
    
    ## iterate all pairs
    for k,v in dic_uniprot_candidate.items():

        ## list for identity info , score 
        list_identity_info = []
        list_identity_score = []

        for each_hit in v:
            ## Run the mapping
            input_file_name = k + '_' + each_hit + '.fasta'
            input_aligned_dir = '/home/bwang/project/cancersnp/data/polyphen2/RawAlignedFasta/'
            input_aligned_file = input_aligned_dir + input_file_name

            out_mapping_file_name = k + '_' + each_hit + '.mapping' 
            out_mapping_dir = '/home/bwang/ResidueMapping/'
            output_mapping_info = out_mapping_dir + out_mapping_file_name
            info , score = ReadIdentity(alignedFasta_file = input_aligned_file,
                                        out_file_path = output_mapping_info,
                                        threshold_iden = threshold_iden_score)
            list_identity_info.append(info)
            list_identity_score.append(score)

        max_score = max(list_identity_score)
        if max_score > threshold_iden_score:
            for each_info in list_identity_info:
                list_info_slices = re.split(',', each_info)
                seq1_iden_score = list_info_slices[-2].upper()
                if float(seq1_iden_score) == float(max_score):
                    print(each_info)
    return 0

if __name__ == "__main__":
    main(candidate_file = 'uniprot_pdb_candidate', threshold_iden_score = 0.80)