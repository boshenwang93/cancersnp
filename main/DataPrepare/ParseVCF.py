#! /usr/bin/python3 
import re

##############################################
#### Reading Coding Mutation Info ############
#### Return as list ##########################
##############################################
def read_VCF_CodingMutation (VCF_file_path):
    # the return list 
    # geneName + Chrosome + ChrPosition + Refer(ATCG) + Alternative(ATCG) 
    # CDS + Peptide 
    list_out_entry = []

    # capture the mutation which classfied as SNP
    list_SNP_line = [] 
    f = open(VCF_file_path, "r")
    for line in f:
        line = line.strip()
        ## read the content, ignore the comment line starting with '#'
        if line.startswith("#") == 0:
            slices = re.split("\t",line)
            details = slices[7].strip()
            details_slices = re.split(";",details)
            for element in details_slices:
                element = element.strip()
                if element == "SNP":
                    list_SNP_line.append(line)
    f.close()

    #### PASS Redundant check, No repeat lines 
    # list_unique = []
    # for e in list_SNP_line:
    #     if e not in list_unique:
    #         list_unique.append(e)
    # print(len(list_unique))

    for single_SNP_line in list_SNP_line:
        current_out_entry = ""

        slices = re.split("\t",single_SNP_line)
        chromosome = slices[0].strip()
        chr_position = slices[1].strip()
        cosmic_mutation_id = slices[2].strip()
        reference_site_N = slices[3].strip()
        alternative_site_N = slices[4].strip()
        
        current_out_entry += cosmic_mutation_id + "," + chromosome + "," +\
                             chr_position + "," + reference_site_N + "," +\
                             alternative_site_N

        details = slices[7].strip()
        details_slices = re.split(";",details)
        
        gene_name = ""
        Ensembl_transcript = ""
        CDS_info = ""
        Peptide_info = ""
        for element in details_slices:
            element = element.strip()

            ## GENE name (may with ENSEMBL Transcript ID)
            if element.startswith("GENE"):
                gene_id_list = re.split("=",element)[1].strip()
                if re.search("_",gene_id_list):
                    gene_name += re.split("_",gene_id_list)[0].strip()
                    Ensembl_transcript += re.split("_",gene_id_list)[1].strip()
                else:
                    gene_name += gene_id_list
            if element.startswith("CDS"):
                CDS_info += re.split("=",element)[1]
            if element.startswith("AA"):
                Peptide_info += re.split("=",element)[1]
        
        current_out_entry += gene_name + "," + CDS_info + "," + Peptide_info
        print(current_out_entry)
    return list_out_entry

VCF_Coding_Mutation_file = "/data/project/SNP/COSMIC/v83/VCF/CosmicCodingMuts.vcf"
read_VCF_CodingMutation(VCF_Coding_Mutation_file)