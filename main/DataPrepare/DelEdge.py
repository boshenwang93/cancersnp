#! /usr/bin/python3

import math
import re 

## Given the VDW radiue for atoms
dictionary_atom_VDWradius = {

}

## Parse PDB file to obtain 3D coordinate info
def capture_heavy_atom(PDB_file_path):
    # list for Heavy atom entry 
    # Residue with seqnum, atom, atom seqnum
    list_heavy_atom_entry = []

    # initalize the dictionary [ATOM] = X/Y/Z
    dictionary_atom_x = {}
    dictionary_atom_y = {}
    dictionary_atom_z = {}

    # reading the PDB file 
    f = open(PDB_file_path, 'r')
    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21].upper()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

    f.close()

    return list_heavy_atom_entry, dictionary_atom_x,\
           dictionary_atom_y, dictionary_atom_z

## Projection 3D to 2D plane
def Projection(list_atoms, dict_x, dict_y, dict_z):
    ## initalize the dictionary[ATOM] = X/Y in 2D plane
    dictionary_atom_x_2D = {}
    dictionary_atom_y_2D = {}



    return dictionary_atom_x_2D, dictionary_atom_y_2D

## BeachLine/SweepLine algorithm 
def BeachLine_Voronoi(list_atoms, dictionary_atom_x_2D, dictionary_atom_y_2D):
    ## initalize list for voronoi edges  '
    list_voronoi_edges =[]


    return list_voronoi_edges 

