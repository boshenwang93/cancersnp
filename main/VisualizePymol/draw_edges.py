#! /usr/bin/python3 

import re

def Get_Edge_Pair(Alpha_Edge_File):
    ## initalize edge pair for inter/intra residue
    list_interRes_edge_pair = []
    list_intraRes_edge_pair = []

    f = open(Alpha_Edge_File, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            iterator = re.finditer(':', line)
            iterator = tuple(iterator)
            count = len(iterator)
            
            if count == 8:
                list_line_slices = re.split(' ', line)
                atom1 = list_line_slices[0]
                atom2 = list_line_slices[1]
            
                list_s_atom1 = re.split(':', atom1)
                atom1_pm = '///'  + list_s_atom1[0] + '/' +\
                       list_s_atom1[1] + '/' +\
                       list_s_atom1[4]
                res_1 = '///'  + list_s_atom1[0] + '/' +\
                       list_s_atom1[1]
                       
                list_s_atom2 = re.split(':', atom2)
                atom2_pm = '///'  + list_s_atom2[0] + '/' +\
                       list_s_atom2[1] + '/' +\
                       list_s_atom2[4]
                res_2 = '///'  + list_s_atom2[0] + '/' +\
                       list_s_atom2[1]
                
                entry = atom1_pm + ',' + atom2_pm
                if res_1 != res_2:
                    list_interRes_edge_pair.append(entry)
                else:
                    list_intraRes_edge_pair.append(entry)
    f.close()
    return list_interRes_edge_pair, list_intraRes_edge_pair

def Get_Surface_Edge_Pair(Surface_Edge_File):
    ## initalize edge pair for surface
    list_surface_edge_pair = []

    f = open(Surface_Edge_File, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            iterator = re.finditer(':', line)
            iterator = tuple(iterator)
            count = len(iterator)
            
            if count == 8:
                list_line_slices = re.split(' ', line)
                atom1 = list_line_slices[0]
                atom2 = list_line_slices[1]
            
                list_s_atom1 = re.split(':', atom1)
                atom1_pm = '///'  + list_s_atom1[0] + '/' +\
                       list_s_atom1[1] + '/' +\
                       list_s_atom1[4]
                res_1 = '///'  + list_s_atom1[0] + '/' +\
                       list_s_atom1[1]
                       
                list_s_atom2 = re.split(':', atom2)
                atom2_pm = '///'  + list_s_atom2[0] + '/' +\
                       list_s_atom2[1] + '/' +\
                       list_s_atom2[4]
                res_2 = '///'  + list_s_atom2[0] + '/' +\
                       list_s_atom2[1]
                
                entry = atom1_pm + ',' + atom2_pm
                list_surface_edge_pair.append(entry)
    f.close()

    return list_surface_edge_pair

def DrawEdgeLine(list_edge_pair):
    from pymol import cmd 

    for each_pair in list_edge_pair:
        a1_info = re.split(',', each_pair)[0]
        a2_info = re.split(',', each_pair)[1]
        
        cmd.select('a1', a1_info)
        cmd.select('a2', a2_info)
        cmd.distance('d1', 'a1', 'a2')
        cmd.set('dash_radius', '0.05')
        cmd.hide('label', 'd1')

    return 0

sample_edge_file = '/home/bwang/project/cancersnp/main/calculation/castp/1A5E_A.alpha.edges'
sample_surface_edge_file = '/home/bwang/project/cancersnp/main/calculation/castp/1A5E_A.surface.atoms.contacts'
list_interRes_edge, list_intraRes_edge = Get_Edge_Pair(Alpha_Edge_File = sample_edge_file)
list_surface_edge = Get_Surface_Edge_Pair(Surface_Edge_File = sample_surface_edge_file)
DrawEdgeLine(list_edge_pair = list_interRes_edge)
DrawEdgeLine(list_edge_pair = list_surface_edge)

