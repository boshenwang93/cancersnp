#! /usr/bin/python3 

import re

def CountContact(DelEdgeFile):

    list_unique_residues = []

    f = open(DelEdgeFile, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            list_line_slices = re.split(' ', line)
            atom1 = list_line_slices[0]
            atom2 = list_line_slices[1]
            
            list_s_atom1 = re.split(':', atom1)
            residue1 = list_s_atom1[0] + ':' +\
                       list_s_atom1[1] + ':' +\
                       list_s_atom1[2]
                       
            list_s_atom2 = re.split(':', atom2)
            residue2 = list_s_atom2[0] + ':' +\
                       list_s_atom2[1] + ':' +\
                       list_s_atom2[2]
            if residue1 not in list_unique_residues:
                list_unique_residues.append(residue1)
            if residue2 not in list_unique_residues:
                list_unique_residues.append(residue2)
    f.close()

    for i in range(0, len(list_unique_residues)):

        for j in range(0, len(list_unique_residues)):
            pair_id_1 = list_unique_residues[i] + list_unique_residues[j]
            pair_id_2 = list_unique_residues[j] + list_unique_residues[i]
            count_pair_edge = 0
        
            f = open(DelEdgeFile, 'r')
            for line in f:
                line = line.strip()
                if line != '':
                    list_line_slices = re.split(' ', line)
                    atom1 = list_line_slices[0]
                    atom2 = list_line_slices[1]
                    list_s_atom1 = re.split(':', atom1)
                    residue1 = list_s_atom1[0] + ':' +\
                               list_s_atom1[1] + ':' +\
                               list_s_atom1[2]
                       
                    list_s_atom2 = re.split(':', atom2)
                    residue2 = list_s_atom2[0] + ':' +\
                               list_s_atom2[1] + ':' +\
                               list_s_atom2[2]
                    
                    current_pair = residue1 + residue2
                    if current_pair == pair_id_1:
                        count_pair_edge += 1 
                    elif current_pair == pair_id_2:
                        count_pair_edge += 1
            f.close()
            
            # out_entry = pair_id + ',' + str(count_pair_edge)
            # print(out_entry)
            print(str(count_pair_edge), end=',')
        print("\n")


    return 0 

CountContact(DelEdgeFile = '1a5e.del.edges')