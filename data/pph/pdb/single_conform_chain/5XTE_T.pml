load 5XTE_T.poc
select pocket_4_0, id 21539+21588+21644+21654+21655+21658+21666+21539+21588+21644+21654+21655+21658+21666;select pocket_4, pocket_4_0;delete pocket_4_0
select pocket_3_0, id 21596+21641+21647+21662+21665+21666+21596+21596+21641+21647+21647+21662+21665+21666+21666;select pocket_3, pocket_3_0;delete pocket_3_0
select pocket_2_0, id 21596+21647+21661+21662+21596+21647+21661+21662;select pocket_2, pocket_2_0;delete pocket_2_0
select pocket_1_0, id 21486+21589+21617+21645+21486+21589+21617+21645;select pocket_1, pocket_1_0;delete pocket_1_0
set transparency=0.35
bg_color white
