load 1OCC_V.poc
select pocket_4_0, id 26896+26907+26910+26936+26939+26896+26896+26907+26910+26910+26936+26936+26939;select pocket_4, pocket_4_0;delete pocket_4_0
select pocket_3_0, id 26871+26873+26896+26907+26936+26871+26873+26896+26907+26936;select pocket_3, pocket_3_0;delete pocket_3_0
select pocket_2_0, id 26388+26389+26420+26423+26388+26389+26420+26423;select pocket_2, pocket_2_0;delete pocket_2_0
select pocket_1_0, id 26777+26794+26798+26824+26777+26794+26798+26824;select pocket_1, pocket_1_0;delete pocket_1_0
set transparency=0.35
bg_color white
