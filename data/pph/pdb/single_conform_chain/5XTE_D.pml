load 5XTE_D.poc
select pocket_5_0, id 2663+2665+2666+2688+2689+2691+2692+2696+2733+2735+2663+2665+2666+2666+2688+2689+2691+2691+2692+2696+2733+2733+2735;select pocket_5, pocket_5_0;delete pocket_5_0
select pocket_4_0, id 3108+3110+3114+3115+3131+3132+3134+3108+3110+3114+3115+3131+3132+3134;select pocket_4, pocket_4_0;delete pocket_4_0
select pocket_3_0, id 3050+3078+3090+3091+3092+3122+3050+3078+3090+3091+3092+3122;select pocket_3, pocket_3_0;delete pocket_3_0
select pocket_2_0, id 2972+2998+3031+3062+3064+2972+2998+3031+3062+3064;select pocket_2, pocket_2_0;delete pocket_2_0
select pocket_1_0, id 2713+2752+2755+2777+2778+2713+2752+2755+2777+2778;select pocket_1, pocket_1_0;delete pocket_1_0
set transparency=0.35
bg_color white
