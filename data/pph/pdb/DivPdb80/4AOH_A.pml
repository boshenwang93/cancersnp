load 4AOH_A.poc
select pocket_12_0, id 18+24+27+29+41+43+46+52+952+953+954+955+956+957+965+974+976+990+18+18+18+18+24+24+24+27+29+29+29+29+41+41+43+43+43+46+46+52+52+952+952+953+954+955+955+956+956+956+957+965;select pocket_12_0 pocket_12_0 or id 974+974+976+990+990+990+990+990;select pocket_12, pocket_12_0;delete pocket_12_0
select pocket_11_0, id 268+270+296+305+306+309+357+359+806+810+816+819+820+840+268+270+270+296+305+305+305+305+306+309+309+309+357+359+359+359+806+806+810+816+816+816+819+819+820+820+820+820+840;select pocket_11, pocket_11_0;delete pocket_11_0
select pocket_10_0, id 40+75+78+81+82+85+88+114+123+125+988+993+998+1002+40+75+78+81+82+82+85+88+114+114+114+123+123+125+988+988+993+993+998+1002;select pocket_10, pocket_10_0;delete pocket_10_0
select pocket_9_0, id 381+755+760+768+769+775+778+779+791+812+817+822+381+755+760+768+769+775+778+779+791+812+817+822;select pocket_9, pocket_9_0;delete pocket_9_0
select pocket_8_0, id 56+59+99+467+468+469+497+499+506+983+56+59+59+99+99+467+468+469+497+497+499+506+506+983;select pocket_8, pocket_8_0;delete pocket_8_0
select pocket_7_0, id 653+908+911+912+918+930+1040+653+908+911+912+918+930+1040;select pocket_7, pocket_7_0;delete pocket_7_0
select pocket_6_0, id 476+478+479+487+514+553+554+476+478+478+478+479+487+514+514+553+553+554+554+554;select pocket_6, pocket_6_0;delete pocket_6_0
select pocket_5_0, id 422+439+922+1009+1050+422+439+922+1009+1050;select pocket_5, pocket_5_0;delete pocket_5_0
select pocket_4_0, id 744+746+846+847+865+744+746+846+847+865;select pocket_4, pocket_4_0;delete pocket_4_0
select pocket_3_0, id 924+1008+1013+1050+924+1008+1013+1050;select pocket_3, pocket_3_0;delete pocket_3_0
select pocket_2_0, id 79+112+333+352+79+112+333+352;select pocket_2, pocket_2_0;delete pocket_2_0
select pocket_1_0, id 745+846+864+865+745+846+864+865;select pocket_1, pocket_1_0;delete pocket_1_0
set transparency=0.35
bg_color white
