#! /usr/bin/python3
import re 
import os
from multiprocessing import Process, Pool

def RunCastp (castp_file):
    ## for pocket contribution 
    poc_cmd = "newcast -fp . " + castp_file
    ## output file focus on PDB.poc

    ## for SASA calculation 
    SASA_cmd = "newcast -fq4 . " + castp_file
    ## output file focus on PDB.4.contrib

    ## for Delone Triangulation Edge Calculation
    del_cmd = "newcast -fd . " + castp_file

    os.system(poc_cmd)
    os.system(SASA_cmd)

    return 0 

def main(assigned_folder):
    # iterating the pdb folder
    list_pdb_file = []

    pdb_directory = assigned_folder
    for root, dirs, files in os.walk(pdb_directory):
        for file in files:
            if file.endswith("pdb"):
                list_pdb_file.append(file)
    return list_pdb_file

if __name__ == "__main__":
    list_pdb = main(assigned_folder = '/home/bwang/project/CancerSNP/data/pph/pdb/DivPdb95/')
    
    with Pool(processes= 3) as pool:
        print(pool.map(RunCastp, list_pdb))
